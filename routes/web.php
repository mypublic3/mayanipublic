<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\SongsController;
use App\Http\Controllers\Admin\ArtistController;
use App\Http\Controllers\Admin\GenreController;
use App\Http\Controllers\Admin\AlbumController;
use App\Http\Controllers\Admin\PlaylistController;
use App\Http\Controllers\Admin\ChartController;
use App\Http\Controllers\Admin\SubadminController;
use App\Http\Controllers\Admin\SettingController;








/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

    Route::get("/dashboard",[SongsController::class,"dashboard"])->name('admin.dashboard');
    Route::get("/users",[SongsController::class,"users"])->name('admin.users');
    Route::post("/saveusers",[SongsController::class,"saveusers"])->name('admin.saveusers');

    Route::get("/profile",[SubadminController::class,"profile"])->name('admin.profile');

    Route::get("/subadmin",[SubadminController::class,"subadmin"])->name('admin.subadmin');

    Route::post("/savesubadmin",[SubadminController::class,"savesubadmin"])->name('admin.savesubadmin');

    Route::get("/artist",[ArtistController::class,"artist"])->name('admin.artist');
    Route::post("/saveartist",[ArtistController::class,"saveartist"])->name('admin.saveartist');
    Route::get("/artistdetail",[ArtistController::class,"artistdetail"])->name('admin.artistdetail');

    Route::get("/genre",[GenreController::class,"genre"])->name('admin.genre');
    Route::post("/savegenre",[GenreController::class,"savegenre"])->name('admin.savegenre');
    Route::get("/album",[AlbumController::class,"album"])->name('admin.album');
    Route::post("/savealbum",[AlbumController::class,"savealbum"])->name('admin.savealbum');
    Route::get("/albumdetail",[AlbumController::class,"albumdetail"])->name('admin.albumdetail');
    Route::post("/savesongtoalbum",[AlbumController::class,"savesongtoalbum"])->name('admin.savesongtoalbum');
    Route::post("/updateorderas",[AlbumController::class,"updateorderas"])->name('admin.updateorderas');


    Route::get("/songs",[SongsController::class,"songs"])->name('admin.songs');
    Route::post("/savesong",[SongsController::class,"savesong"])->name('admin.savesong');

    Route::get("/playlist",[PlaylistController::class,"playlist"])->name('admin.playlist');
    Route::post("/saveplaylist",[PlaylistController::class,"saveplaylist"])->name('admin.saveplaylist');
    Route::get("/playlistdetail",[PlaylistController::class,"playlistdetail"])->name('admin.playlistdetail');

    Route::post("/savesongtoplaylist",[PlaylistController::class,"savesongtoplaylist"])->name('admin.savesongtoplaylist');
    Route::post("/updateorderps",[PlaylistController::class,"updateorderps"])->name('admin.updateorderps');

    Route::get("/chart",[ChartController::class,"chart"])->name('admin.chart');
    Route::post("/savechart",[ChartController::class,"savechart"])->name('admin.savechart');
    Route::get("/subscription",[ChartController::class,"subscription"])->name('admin.subscription');
    Route::post("/savesubscription",[ChartController::class,"savesubscription"])->name('admin.savesubscription');

    Route::get("/company-setting",[SettingController::class,"companysetting"])->name('admin.companysetting');
    Route::post("/savesetting",[SettingController::class,"savesetting"])->name('admin.savesetting');

    Route::get("/cms-setting",[SettingController::class,"cmssetting"])->name('admin.cmssetting');
    Route::post("/savecmssetting",[SettingController::class,"savecmssetting"])->name('admin.savecmssetting');
    Route::get("/feedback",[SettingController::class,"feedback"])->name('admin.feedback');
    Route::get("/share-quote",[SettingController::class,"sharequote"])->name('admin.sharequote');
    Route::post("/saveshare",[SettingController::class,"saveshare"])->name('admin.saveshare');



});
