<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\UserPlaylistController;




/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('countries', [AuthController::class, 'countries'])->name('api.countries');


Route::post('createuserMobile', [AuthController::class, 'createuserMobile'])->name('authapi.createuserMobile');
Route::post('createuserPass', [AuthController::class, 'createuserPass'])->name('authapi.createuserPass');
// forgot password api 
Route::post('forgetpass', [AuthController::class, 'forgetpass'])->name('authapi.forgetpass');
Route::post('forgetpassverify', [AuthController::class, 'forgetpassverify'])->name('authapi.forgetpassverify');


Route::post('resendotp', [AuthController::class, 'resendotp'])->name('authapi.resendotp');

Route::post('mobileVerify', [AuthController::class, 'mobileVerify'])->name('authapi.mobileVerify');
Route::post('userLogin', [AuthController::class, 'userLogin'])->name('authapi.userLogin');


Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('updatetoken', [AuthController::class, 'updatetoken'])->name('authapi.updatetoken');

   

    Route::get('prefrences', [HomeController::class, 'prefrences'])->name('api.prefrences');

    Route::get('artistlist', [HomeController::class, 'artistlist'])->name('app.artistlist');
    Route::get('genrelist', [HomeController::class, 'genrelist'])->name('api.genrelist');
    Route::get('albumlist', [HomeController::class, 'albumlist'])->name('api.albumlist');
    Route::post('albumsongs', [HomeController::class, 'albumsongs'])->name('api.albumsongs');

    Route::get('playlist', [HomeController::class, 'playlist'])->name('api.playlist');
    Route::post('playlistsongs', [HomeController::class, 'playlistsongs'])->name('api.playlistsongs');


    Route::get('songlist', [HomeController::class, 'songlist'])->name('api.songlist');
    Route::get('trendingsong', [HomeController::class, 'trendingsong'])->name('api.trendingsong');
    Route::post('songplay', [HomeController::class, 'songplay'])->name('api.songplay');
    Route::post('artistsongs', [HomeController::class, 'artistsongs'])->name('api.artistsongs');
    Route::post('search', [HomeController::class, 'search'])->name('api.search');
    Route::get('chartlist', [HomeController::class, 'chartlist'])->name('api.chartlist');
    Route::post('chartsongs', [HomeController::class, 'chartsongs'])->name('api.chartsongs');

    Route::post('userplaylist', [HomeController::class, 'userplaylist'])->name('api.userplaylist');
    Route::post('saveprefrence', [HomeController::class, 'saveprefrence'])->name('api.saveprefrence');

    Route::get('getuserplaylist', [HomeController::class, 'getuserplaylist'])->name('api.getuserplaylist');
    Route::get('recommendedsongs', [HomeController::class, 'recommendedsongs'])->name('api.recommendedsongs');
    Route::post('genresongs', [HomeController::class, 'genresongs'])->name('api.genresongs');


    Route::post('removeuserplaylist', [UserPlaylistController::class, 'removeuserplaylist'])->name('api.removeuserplaylist');
    Route::get('userlikedsong', [UserPlaylistController::class, 'userlikedsong'])->name('api.userlikedsong');
    Route::post('likesong', [UserPlaylistController::class, 'likesong'])->name('api.likesong');
    Route::post('addplaylistsong', [UserPlaylistController::class, 'addplaylistsong'])->name('api.addplaylistsong');
    Route::post('userplaylistsong', [UserPlaylistController::class, 'userplaylistsong'])->name('api.userplaylistsong');
    Route::post('removeuserplaylistsong', [UserPlaylistController::class, 'removeuserplaylistsong'])->name('api.removeuserplaylistsong');
    Route::post('checklikesong', [UserPlaylistController::class, 'checklikesong'])->name('api.checklikesong');
    // api for user to like playlists and albums
    Route::get('getlikeplaylists', [UserPlaylistController::class, 'getlikeplaylists'])->name('api.getlikeplaylists');
    Route::post('likeplaylists', [UserPlaylistController::class, 'likeplaylists'])->name('api.likeplaylists');
    Route::post('checklikeplaylist', [UserPlaylistController::class, 'checklikeplaylist'])->name('api.checklikeplaylist');
    Route::get('deleteuser', [UserPlaylistController::class, 'deleteuser'])->name('api.deleteuser');

    Route::post('songplay', [HomeController::class, 'songplay'])->name('api.songplay');
    Route::post('downloadsong', [HomeController::class, 'downloadsong'])->name('api.downloadsong');



    Route::get('getuserprefrence', [HomeController::class, 'getuserprefrence'])->name('api.getuserprefrence');

    // setting api 
    Route::get('aboutus', [HomeController::class, 'aboutus'])->name('api.aboutus');
    Route::get('termsconditions', [HomeController::class, 'termsconditions'])->name('api.termsconditions');
    Route::get('privacypolicy', [HomeController::class, 'privacypolicy'])->name('api.privacypolicy');
    Route::get('legaldisclaimer', [HomeController::class, 'legaldisclaimer'])->name('api.legaldisclaimer');

    Route::get('userprofile', [HomeController::class, 'userprofile'])->name('authapi.userprofile');
    Route::post('updateprofile', [HomeController::class, 'updateprofile'])->name('authapi.updateprofile');
    Route::post('updateprofileimg', [HomeController::class, 'updateprofileimg'])->name('authapi.updateprofileimg');
    Route::post('feedback', [HomeController::class, 'feedback'])->name('api.feedback');
    Route::get('notifications', [HomeController::class, 'notifications'])->name('api.notifications');
    Route::get('sharecontent', [HomeController::class, 'sharecontent'])->name('api.sharecontent');
    Route::post('followartist', [HomeController::class, 'followartist'])->name('api.followartist');

    Route::get('logout', [HomeController::class, 'logout'])->name('api.logout');

    



});
