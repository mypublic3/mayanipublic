<div>
    <!-- Waste no more time arguing what a good man should be, be one. - Marcus Aurelius -->
    @push('head')
    
        <!-- DataTables -->
        <link href="{{asset('admindash')}}/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="{{asset('admindash')}}/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Select datatable -->
        <link href="{{asset('admindash')}}/libs/datatables.net-select-bs4/css/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable -->
        <link href="{{asset('admindash')}}/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />     




    @endpush

    @push('body')
    
        <!-- Required datatable js -->
        <script src="{{asset('admindash')}}/libs/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="{{asset('admindash')}}/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

        <!-- Buttons examples -->
        <script src="{{asset('admindash')}}/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="{{asset('admindash')}}/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
        <script src="{{asset('admindash')}}/libs/jszip/jszip.min.js"></script>
        <script src="{{asset('admindash')}}/libs/pdfmake/build/pdfmake.min.js"></script>
        <script src="{{asset('admindash')}}/libs/pdfmake/build/vfs_fonts.js"></script>
        <script src="{{asset('admindash')}}/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="{{asset('admindash')}}/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="{{asset('admindash')}}/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>
        <script src="{{asset('admindash')}}/libs/datatables.net-keyTable/js/dataTables.keyTable.min.html"></script>
        <script src="{{asset('admindash')}}/libs/datatables.net-select/js/dataTables.select.min.js"></script>
        
        <!-- Responsive examples -->
        <script src="{{asset('admindash')}}/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="{{asset('admindash')}}/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

        <!-- Datatable init js -->
        <script src="{{asset('admindash')}}/js/pages/datatables.init.js"></script>



    @endpush

</div>