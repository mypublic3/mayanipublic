<!doctype html>
<html lang="en">


<!-- Mirrored from www.preview.pichforest.com/samply/layouts/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2022 17:09:27 GMT -->

<head>

    <meta charset="utf-8" />
    <title> @yield('title') | Mayani Music - Admin Panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Mayani music" name="description" />
    <meta content="Pichforest" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('admindash')}}/images/favicon.ico">

    <!-- Bootstrap Css -->
    <link href="{{asset('admindash')}}/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{asset('admindash')}}/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{asset('admindash')}}/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
    <!-- Dark Mode Css-->
    <link href="{{asset('admindash')}}/css/dark-layout.min.css" id="app-style" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
       
<style type="text/css">
.hidden {
visibility: hidden;
display: none;
}
#pageloader  
{  
  background: rgba( 255, 255, 255, 0.8 );  
  display: none;  
  height: 100%;  
  position: fixed;  
  width: 100%;  
  z-index: 9999;  
}  
  
#pageloader img  
{  
  left: 50%;  
  margin-left: -32px;  
  margin-top: -32px;  
  position: absolute;  
  top: 50%;  
}  
    </style>
    @stack('head')

</head>
<!-- style="background-color:#063970;" -->
<body data-sidebar="dark" style="background-color:#17273a;">

    <!-- <body data-layout="horizontal" data-topbar="dark"> -->

    <!-- Begin page -->
    <div id="layout-wrapper" >


        <header id="page-topbar"style="background-color:#17273a;" >
            <div class="navbar-header" >
                <div class="d-flex">
                    <!-- LOGO -->
                    <div class="navbar-brand-box">
                        <a href="index.html" class="logo logo-dark">
                            <span class="logo-sm">
                                    <img src="{{asset('admindash')}}/mayanilogo.png" alt="logo-sm" height="22">
                                </span>
                            <span class="logo-lg">
                                    <img src="{{asset('admindash')}}/mayanilogo.png" alt="logo-dark" height="23">
                                </span>
                        </a>

                        <a href="#" class="logo logo-light">
                            <span class="logo-sm">
                                    <img src="{{asset('admindash')}}/mayanilogo.png" alt="logo-sm-light" height="22">
                                </span>
                            <span class="logo-lg">
                                    <img src="{{asset('admindash')}}/mayanilogo.png" alt="logo-light" height="23">
                                </span>
                        </a>
                    </div>

                    <button type="button" class="btn btn-sm px-3 font-size-16 vertinav-toggle header-item waves-effect" id="vertical-menu-btn">
                            <i class="fa fa-fw fa-bars"></i>
                        </button>

                    <button type="button" class="btn btn-sm px-3 font-size-16 horinav-toggle header-item waves-effect waves-light" data-bs-toggle="collapse" data-bs-target="#topnav-menu-content">
                            <i class="fa fa-fw fa-bars"></i>
                        </button>

                  
                </div>

                <div class="d-flex" >



                    <div class="dropdown d-none d-lg-inline-block ms-1">
                        <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                                <i class="mdi mdi-fullscreen"></i>
                            </button>
                    </div>


                    <div class="dropdown d-inline-block">
                        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle header-profile-user" src="{{asset('admindash')}}/images/users/avatar-1.jpg"
                                    alt="Header Avatar">
                                <span class="d-none d-xl-inline-block ms-1 text-light">Admin</span>
                                <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                            </button>
                        <div class="dropdown-menu dropdown-menu-end">
                            <!-- item-->
                            <h6 class="dropdown-header">Welcome Admin!</h6>
                            <a class="dropdown-item" href="{{route('admin.profile')}}"><i class="mdi mdi-profile text-muted font-size-16 align-middle me-1"></i> <span class="align-middle" key="t-logout">Profile</span></a>
                            <a class="dropdown-item" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="mdi mdi-logout text-muted font-size-16 align-middle me-1"></i> <span class="align-middle" key="t-logout">Logout</span></a>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                        </div>
                    </div>

                    <!-- <div class="dropdown d-inline-block">
                        <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                                <i class="bx bx-cog bx-spin"></i>
                            </button>
                    </div> -->

                </div>
            </div>
        </header>

        <!-- ========== Left Sidebar Start ========== -->
        <div class="vertical-menu">

            <div data-simplebar class="h-100">

                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <!-- Left Menu Start -->
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <li class="menu-title" key="t-menu">Menu</li>

                      
                        <li>
                            <a href="{{route('admin.dashboard')}}" class="waves-effect">
                            <i class='bx bxs-dashboard'></i>
                                <span key="t-ui-elements">Dashboard</span>
                            </a>
                        </li>
                        <?php
                          if(Auth::user()->role=='admin'){
                                                      ?>
                        <li>
                            <a href="{{route('admin.subadmin')}}" class="waves-effect">
                                <i class='bx bx-user'></i>
                                <span key="t-ui-elements">Sub Admin</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.users')}}" class="waves-effect">
                                <i class='bx bx-user'></i>
                                <span key="t-ui-elements">Users</span>
                            </a>
                        </li>
                      
                        <li>
                            <a href="{{route('admin.genre')}}" class="waves-effect">
                                <i class='bx bx-music'></i>
                                <span key="t-ui-elements">Genres </span>
                            </a>
                        </li>
                        <?php } ?>
                        <li>
                            <a href="{{route('admin.artist')}}" class="waves-effect">
                                <i class='bx bx-user'></i>
                                <span key="t-ui-elements">Artists </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.album')}}" class="waves-effect">
                                <i class='bx bx-music'></i>
                                <span key="t-ui-elements">Album </span>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('admin.songs')}}" class="waves-effect">
                                <i class='bx bx-music'></i>
                                <span key="t-ui-elements">Songs</span>
                            </a>
                        </li>
                        <?php
                          if(Auth::user()->role=='admin'){
                                                      ?>
                        <li>
                            <a href="{{route('admin.playlist')}}" class="waves-effect">
                                <i class='bx bx-music'></i>
                                <span key="t-ui-elements">Playlists </span>
                            </a>
                        </li>

                        <!-- <li>
                            <a href="#" class="waves-effect">
                                <i class='bx bx-chart'></i>
                                <span key="t-ui-elements">Recommendation </span>
                            </a>
                        </li> -->
                        <li>
                            <a href="{{route('admin.chart')}}" class="waves-effect">
                                <i class='bx bx-chart'></i>
                                <span key="t-ui-elements">Charts </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.subscription')}}" class="waves-effect">
                                <i class='bx bx-money'></i>
                                <span key="t-ui-elements">Subscription </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.sharequote')}}" class="waves-effect">
                                <i class='bx bx-share'></i>
                                <span key="t-ui-elements">Share </span>
                            </a>
                        </li>

                       

                        <li>
                            <a href="{{route('admin.feedback')}}" class="waves-effect">
                                <i class='bx bx-message'></i>
                                <span key="t-ui-elements">Feedbacks </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.cmssetting')}}" class="waves-effect">
                                <i class='bx bx-message'></i>
                                <span key="t-ui-elements">Cms Setting </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.companysetting')}}" class="waves-effect">
                                <i class='bx bx-message'></i>
                                <span key="t-ui-elements">Setting </span>
                            </a>
                        </li>
                       
                        <?php } ?>
                       

                      

                      
                      
                      





                    </ul>
                </div>
                <!-- Sidebar -->
            </div>
        </div>
        <!-- Left Sidebar End -->
                <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content" >

            <div class="page-content" >
                <div class="container-fluid" >

                    <!-- start page title -->
                    <div class="row" >
                        <div class="col-12" >
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between" style="background-color:#17273a;">
                                <h4 class="mb-sm-0 font-size-18"> @yield('title')</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <!-- <li class="breadcrumb-item"><a href="javascript: void(0);">Mayani</a></li> -->
                                        <li class="breadcrumb-item active"> @yield('title')</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

        @yield('content')

       

        </div>
        <!-- end main content-->
      
    </div>
    <!-- END layout-wrapper -->
    <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <script>
                                document.write(new Date().getFullYear())
                            </script> © Mayani .
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-end d-none d-sm-block">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div id="pageloader">  
   <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />  
</div>  


    


    <!-- JAVASCRIPT -->
    <script src="{{asset('admindash')}}/libs/jquery/jquery.min.js"></script>
    <script src="{{asset('admindash')}}/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('admindash')}}/libs/metismenu/metisMenu.min.js"></script>
    <script src="{{asset('admindash')}}/libs/simplebar/simplebar.min.js"></script>
    <script src="{{asset('admindash')}}/libs/node-waves/waves.min.js"></script>

    <!-- apexcharts -->
    <script src="{{asset('admindash')}}/libs/apexcharts/apexcharts.min.js"></script>

    <!-- dashboard init -->
    <script src="{{asset('admindash')}}/js/pages/dashboard.init.js"></script>

    <!-- App js -->
    <script src="{{asset('admindash')}}/js/app.js"></script>
    <script src="{{asset('admindash')}}/js/sweetalert.min.js"></script>


    @stack('body')
    @stack('script')


</body>


<!-- Mirrored from www.preview.pichforest.com/samply/layouts/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2022 17:09:47 GMT -->

</html>