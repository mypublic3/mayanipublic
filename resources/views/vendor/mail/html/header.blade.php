@props(['url'])
<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Mayani Music')
<img src="{{asset('admindash')}}/mayanilogo.png" class="logo" alt="Mayani  Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
