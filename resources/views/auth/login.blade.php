<!doctype html>
<html lang="en">

    
<!-- Mirrored from www.preview.pichforest.com/samply/layouts/auth-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2022 17:10:08 GMT -->
<head>
        
        <meta charset="utf-8" />
        <title>Login | Mayani Music - Admin Panel</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Login Page" name="Myani music" />
        <meta content="Pichforest" name="author" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('admindash')}}/images/favicon.ico">

        <!-- Bootstrap Css -->
        <link href="{{asset('admindash')}}/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="{{asset('admindash')}}/css/icons.min.css" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{asset('admindash')}}/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
        <!-- Dark Mode Css-->
        <link href="{{asset('admindash')}}/css/dark-layout.min.css" id="app-style" rel="stylesheet" type="text/css" />

    </head>

    <body data-sidebar="light" style="background:black;">

    <!-- <body data-layout="horizontal" data-topbar="dark"> -->
        <div class="account-pages my-5 pt-sm-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="text-center mb-5">
                            <a href="index.html" class="auth-logo">
                                <img src="{{asset('admindash')}}/mayanilogo.png" alt="Logo Dark" height="28" class="auth-logo-dark">
                                <img src="{{asset('admindash')}}/mayanilogo.png" alt="Logo Light" height="28" class="auth-logo-light">
                            </a>
                            <p class="font-size-15 text-muted mt-3"> <b></b> Admin Login</p>
                        </div>
                        <div class="card overflow-hidden">
                            <div class="row g-0">
                                <div class="col-lg-6">
                                    <div class="p-lg-5 p-4">

                                        <div>
                                            <h5>Welcome Back !</h5>
                                            <p class="text-muted">Sign in to continue to Samply.</p>
                                        </div>
                                    
                                        <div class="mt-4 pt-3">
                                        <form action="{{ route('login') }}" method="post" id="loginform" role="form">
                                        @csrf
            
                                                <div class="mb-3">
                                                    <label for="username" class="fw-semibold">Email</label>
                                                    <input type="text" class="form-control" name="email"  id="username" placeholder="Enter username">
                                                    <span class="text-danger e-email" id="e-email"></span>

                                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                                </div>
                        
                                                <div class="mb-3 mb-4">
                                                    <label for="userpassword" class="fw-semibold">Password</label>
                                                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
                                                    <span class="text-danger e-password" id="e-password"></span>
                                                    
   @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>

                                @enderror
                                                </div>
    
                                                <div class="row align-items-center">
                                                    <div class="col-6">
                                                        <div class="form-check">    
                                                            <input type="checkbox" class="form-check-input font-size-16" id="remember-check">
                                                            <label class="form-check-label" for="remember-check">Remember me</label>
                                                        </div>
                                                    </div>  
                                                    <div class="col-6">
                                                        <div class="text-end">
                                                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                                                        </div>
                                                    </div>
                                                </div>
                        
                                                <div class="mt-4">
                                                    <!-- <a href="auth-recoverpw.html" class="text-muted"><i class="mdi mdi-lock me-1"></i> Forgot your password?</a> -->
                                                </div>
                                            </form>
                                        </div>
                    
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="p-lg-5 p-4 bg-auth h-100 d-none d-lg-block">
                                        <div class="bg-overlay"></div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- end card -->
                        <div class="mt-5 text-center">
                          
                            <p>© <script>document.write(new Date().getFullYear())</script> <b>Mayani Music</b></p>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end account page -->
        

        <!-- JAVASCRIPT -->
        <script src="{{asset('admindash')}}/libs/jquery/jquery.min.js"></script>
        <script src="{{asset('admindash')}}/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="{{asset('admindash')}}/libs/metismenu/metisMenu.min.js"></script>
        <script src="{{asset('admindash')}}/libs/simplebar/simplebar.min.js"></script>
        <script src="{{asset('admindash')}}/libs/node-waves/waves.min.js"></script>

        <script src="{{asset('admindash')}}/js/app.js"></script>
        <script>

$("#loginform").submit(function(e) {
    e.preventDefault();
    $('.errors').hide();
    var form = $(this);
    $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: form.serialize(),
        success: function(r) {
            window.location.reload();
        },
        error: function(r) {
            if (r.status == 403) {
                window.location.reload();
            }

            $.each(r.responseJSON.errors, function(i, f) {
                $('#e-' + i).show();
                $('#e-' + i).text(f);
            });

        }

    })
});
</script>


    </body>

<!-- Mirrored from www.preview.pichforest.com/samply/layouts/auth-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 15 Aug 2022 17:10:08 GMT -->
</html>
