@extends('layouts.admin')
@section('title', 'Album')

@section('content')




                    <div class="row">
                    <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"> Album Detail </h4>
                                        <br>
                                        <!-- <button type="button" class="btn btn-primary waves-effect waves-light mb-4" onclick="addnew()">Add Song</button> -->
                                        <a  href="{{route('admin.album')}}"> <button type="button" class="btn btn-primary waves-effect waves-light mb-4" >Back</button></a>
                                        <table id="Datatable" class="table table-responsive text-center table table-bordered dt-responsive  nowrap w-100" style="width:100% background-color:#17273a; color:#fff;">
                            <thead>
                                <tr>
                                <th>Sr No.</th>

                                    <th>Photo</th>
                                    <th> Title</th>
                                    <th>Artist</th> 
                                    <th>Album</th> 
                                    <th>Play</th>                                  
                                </tr>
                                <tbody id="tablecontents">
                    @foreach($albumsong as $as)
                    <tr class="row1" data-id="{{ $as->id }}">
                      <td>
                        <div style="color:rgb(124,77,255); padding-left: 10px; float: left; font-size: 20px; cursor: pointer;" title="change display order">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                        </div>
                      </td>
                      <td><img width='80px' height='50px' class='img'   src='{{url("/")}}/storage{{$as->songdetail->image}}' /></td>
                        
                      <td>{{$as->songdetail->name}}</td>
                      <?php $art=json_decode($as->artists); ?>
                      <td>{{$art}}</td>
                      <td>{{$as->songdetail_count}}</td>
                      <td><audio controls>
                <source src='{{url("/")}}/storage{{$as->songdetail->track_file}}' type='audio/mpeg'> 
                </audio></td>



                
                    </tr>
                    @endforeach
                  </tbody> 
                           
                             

                            </tbody>
                        </table> 
                        <x-datatables />
                                       
        
</div>
</div>
</div>
                     
                    </div>
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            <!-- addnew modal  -->
            <!-- <div class="modal fade"  tabindex="-1" aria-labelledby="exampleModalLabel"
  data-bs-backdrop="static" aria-hidden="true" id="formodal"> -->
  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" id="formmodal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Song</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="{{ route('admin.savesongtoalbum') }}" method="post" id="form" enctype="multipart/form-data">
                        @csrf
        <div class="modal-body p-4 bg-light">
        
       
         
<div class="row" >
  <div class="col-lg">
  <input type="hidden" name="albumid" value="{{$aid}}" id="albumid">

              <label for="name">Song</label>
              <select class="select2-multiple form-control" name="song" 
                id="select2Multiple" style="width: 100%">
                  <option selected>** Select **</option>
                  @foreach($song as $s)
                  <option value="{{$s->id}}">{{$s->name}}</option>

                  @endforeach
              </select>

                                <span class="text-danger errors " id="e-song"></span>  
                                      </div>
        

        
       
         <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" id="add_btn" class="btn btn-primary">Add </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- modal end  -->

<!-- full size image modal 
 -->
 
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true" id="imgmodal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Full Size Image</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                </div>
                <div class="modal-body">
                    <img src="" id="srclink" class="img-fluid" alt="" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
            @endsection
            @push('script')
            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

            <script>
                 
    
 

   function sendOrderToServer() {

var order = [];
$('tr.row1').each(function(index,element) {
  order.push({
  id: $(this).attr('data-id'),
  position: index+1
 });
 });

$.ajax({
  type: "POST", 
  dataType: "json", 
  url: "{{ route('admin.updateorderas')}}",
  data: {
  order:order,
  _token: '{{csrf_token()}}'
 },
   success: function(response) {
   if (response.status == "success") {
    console.log(response);
   } else {
  console.log(response);
 }
 }
});

}
$(document).ready(function() {
 $('#Datatable').DataTable({pageLength:1000,autoWidth: false
});
 $("#tablecontents").sortable({
           items: "tr",
           cursor: 'move',
            opacity: 0.6,
            update: function() {
           sendOrderToServer();
            }
});
} );
               
              
                function addnew(data=null){
                    $('.errors').hide();
            if (data == null) {
                $("#exampleModalLabel").text("Add  New Song");
                $("#add_btn").text("Add");
                $("#hid").val('');
                $("#form")[0].reset();
              

            } else {
                $("#exampleModalLabel").text("Update");
                $("#add_btn").text("Update");


                $(`#form input[name=id]`).val(data.id);

                $(`#name`).val(data.name);
                $(`#date`).val(data.date);

                pimage.src = "{{url('/')}}/storage/"+data.image;




             
            }
            $("#formmodal").modal("toggle");
        }
        var aid=$("#albumid").val();
        console.log(albumid);
        var loaded=0;

         function reload() {
            if (loaded) {
                $("#Datatable").DataTable().ajax.reload();
                $(".modal").modal('hide');
            } else {
                loaded = 1;
                $("#Datatable").DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.albumdetail') }}/?aid="+aid,
                    columns: [{
                            data: 'id',
                            render: function(data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }                        },
                            {
                            data: 'image',
                            name: 'image'
                        },
                        {
                            data: 'songdetail.name',
                            name: 'songdetail.name'
                        },
                        {
                            data: 'artists',
                            name: 'artists'
                        },
                        {
                            data: 'songdetail_count',
                            name: 'songdetail_count'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },
                    
                    ]
                });
               
            }

        }
                

                // form submit 

                $("#form").submit(function(e) {
            e.preventDefault();
            $('.errors').hide();
            var form = $(this);
            var formdata = new FormData(form[0]);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formdata,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function(r) {
                    
         
                   
                    addnew();
                      window.location.reload();                   

                  
                },
                error: function(r) {
                    $.each(r.responseJSON.errors, function(i, f) {
                        $('#e-' + i).show();
                        $('#e-' + i).text(f);
                    });

                }

            })

        });

                 
        window.onload = function() {
            // reload();
        }
       

        function deletealbumsong(id){
            $.get("{{ route('admin.albumdetail') }}/?aid="+aid+"&did=" + id, function(r) {
                window.location.reload();

 console.log(r);


});
        }
        function openimage(src) {
            $("#imgmodal").modal("toggle");

            $("#srclink").attr("src", src);
        }
      
      


</script>
            @endpush

          