@extends('layouts.admin')
@section('title', 'CMS Setting')

@section('content')




<div class="row">
                    <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"> CMS Setting </h4>
                                        <form action="{{ route('admin.savecmssetting') }}" method="post" id="form" enctype="multipart/form-data">
                        @csrf                                      
                                        <br>
                                        <input type="hidden" name="id" id="id">

                                     

                            <!-- social setting div  -->
                            <div class="col-md-12 socialdiv" id="socialldiv">
                                            <div class="row">
                                            <div class="col-md-12 bg-blue p-4">
                                            <p class="text-light">CMS Setting</p><div class="row">
                                          
<div class="position-relative col-md-12 form-group mb-2">
                                            <div class="row">
                                            <div class="position-relative col-md-12 form-group mb-4">                                            <label class="form-label text-light">About Us*</label>
                                            <textarea class="form-control summernote" name="aboutus" type="text"  id="aboutus"></textarea>

                                            <span class="text-danger errors " id="e-aboutus"></span>  

                                        </div>  
                                      
                                      
</div></div>
                                     
                                        <div class="position-relative col-md-12 form-group mb-4">
                                            <div class="row">
                                            <div class="position-relative col-md-12 form-group mb-4">                                            <label class="form-label text-light">Terms & Conditions *</label>
                                            <textarea class="form-control summernote" type="text" name="terms_conitions" id="terms_conitions"></textarea>

                                            <span class="text-danger errors " id="e-terms_conitions"></span>  

                                        </div>  
                                           
                                      
</div>
</div>
<div class="position-relative col-md-12 form-group mb-4">
                                            <div class="row">
                                            <div class="position-relative col-md-12 form-group mb-4">                                            <label class="form-label text-light">Privacy Policy *</label>
                                            <textarea class="form-control summernote" type="text" name="privacy" id="privacy"></textarea>

                                            <span class="text-danger errors " id="e-privacy"></span>  

                                        </div>  
                                           
                                      
</div>
</div>   <div class="position-relative col-md-12 form-group mb-4">
                                            <div class="row">
                                            <div class="position-relative col-md-12 form-group mb-4">                                            <label class="form-label text-light">Legel Disclaimer *</label>
                                            <textarea class="form-control summernote" type="text" name="legel_disclaimer" id="legel_disclaimer"></textarea>

                                            <span class="text-danger errors " id="e-legel_disclaimer"></span>  

                                        </div>  
                                           
                                      
</div>
</div>   <div class="position-relative col-md-12 form-group mb-4">
                                            <div class="row">
                                            <div class="position-relative col-md-12 form-group mb-4">                                            <label class="form-label text-light">THow To Pay Via Mobile *</label>
                                            <textarea class="form-control summernote" type="text" name="how_to_pay" id="how_to_pay"></textarea>

                                            <span class="text-danger errors " id="e-how_to_pay"></span>  

                                        </div>  
                                           
                                      
</div>
</div>


                                        <div class="col-md-12 form-group text-center mt-2 mb-2">
                                            <button class="btn btn-primary btn-lg" type="submit">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end social  -->
                        </div>
                                      
                                       
        
</div>
</div>
</form>
</div>
                     
                    </div>
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
           

<!-- full size image modal 
 -->
 
            @endsection
            @push('script')
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

            <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<!-- <script type="text/javascript">
    $('.summernote').summernote();
</script> -->

            <script>
$(document).ready(function() {
  $('.summernote').summernote();
});
         
            
                

                // form submit 

                $("#form").submit(function(e) {
            e.preventDefault();
            $('.errors').hide();
            var form = $(this);
            var formdata = new FormData(form[0]);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formdata,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function(r) {
                    
         
                    new Swal(
                  'Success',
                  'Data updated Successfully 👍',
                  'success'
                )
                   edit();    

                  
                },
                error: function(r) {
                    $.each(r.responseJSON.errors, function(i, f) {
                        $('#e-' + i).show();
                        $('#e-' + i).text(f);
                    });

                }

            })

        });

                 
       
        function edit(){
            var id = 1;
            $.get("{{ route('admin.cmssetting') }}/?id=" + id, function(r) {
                $(`#aboutus`).summernote('code', JSON.parse(JSON.stringify(r.aboutus)));
                $('#terms_conitions').summernote('code', JSON.parse(JSON.stringify(r.terms_conitions)));
                $('#privacy').summernote('code', JSON.parse(JSON.stringify(r.privacy)));
                $('#legel_disclaimer').summernote('code', JSON.parse(JSON.stringify(r.legel_disclaimer)));
                $('#how_to_pay').summernote('code', JSON.parse(JSON.stringify(r.how_to_pay)));



});
        }
        window.onload = function() {
            edit();
        }

     
      
      


</script>
            @endpush

          