@extends('layouts.admin')
@section('title', 'Dashboard')

@section('content')


        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->

                    <!-- end page title -->

                    <div class="row" style="background-color:#17273a;">
                        <div class="col-md-6 col-xl-3">
                            <div class="card" >
                                <div class="card-body">
                                    <div class="float-end">
                                        <div class="avatar-sm mx-auto mb-4">
                                            <span class="avatar-title rounded-circle bg-light font-size-24">
                                                    <i class="mdi mdi-music text-primary"></i>
                                                </span>
                                        </div>
                                    </div>
                                    <div>
                                        <p class="text-muted text-uppercase fw-semibold">Total Songs</p>
                                        <h4 class="mb-1 mt-1"><span class="counter-value" data-target="{{$songs}}">{{$songs}}</span></h4>
                                    </div>
                                    <p class="text-muted mt-3 mb-0">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- end col-->

                        <div class="col-md-6 col-xl-3 ">
                            <div class="card">
                                <div class="card-body">
                                    <div class="float-end">
                                        <div class="avatar-sm mx-auto mb-4">
                                            <span class="avatar-title rounded-circle bg-light font-size-24">
                                                    <i class="mdi mdi-music text-success"></i>
                                                </span>
                                        </div>
                                    </div>
                                    <div>
                                        <p class="text-muted text-uppercase fw-semibold">Total Artists</p>
                                        <h4 class="mb-1 mt-1"><span class="counter-value" data-target="{{$artists}}">{{$artists}}</span></h4>
                                    </div>
                                    <p class="text-muted mt-3 mb-0">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- end col-->
                        <?php if(Auth::user()->role=='admin'){
                            ?>
                        <div class="col-md-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="float-end">
                                        <div class="avatar-sm mx-auto mb-4">
                                            <span class="avatar-title rounded-circle bg-light font-size-24">
                                                    <i class="mdi mdi-account-group text-primary"></i>
                                                </span>
                                        </div>
                                    </div>
                                    <div>
                                        <p class="text-muted text-uppercase fw-semibold">Active Users</p>
                                        <h4 class="mb-1 mt-1"><span class="counter-value" data-target="{{$users}}">{{$users}}</span></h4>
                                    </div>
                                    <p class="text-muted mt-3 mb-0">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <!-- end col-->

                        <!-- <div class="col-md-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="float-end">
                                        <div class="avatar-sm mx-auto mb-4">
                                            <span class="avatar-title rounded-circle bg-light font-size-24">
                                                    <i class="mdi mdi-download text-success"></i>
                                                </span>
                                        </div>
                                    </div>
                                    <div>
                                        <p class="text-muted text-uppercase fw-semibold">All Time Downloads</p>
                                        <h4 class="mb-1 mt-1"><span class="counter-value" data-target="9582">0</span></h4>
                                    </div>
                                    <p class="text-muted mt-3 mb-0">
                                    </p>
                                </div>
                            </div>
                        </div> -->
                        <!-- end col-->
                    </div>
                    <!-- end row-->
                   
                   
                    <!-- end row -->

                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="float-end">
                                       
                                    </div>

                                    <h4 class="card-title mb-4">Recent Users</h4>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-borderless align-middle table-centered table-nowrap mb-0">
                                            <thead class="table-light">
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Job title</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Status</th>
                                                    <th colspan="2">Location</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                -->
                                                <!-- end /tr -->
                                            <!-- </tbody>
                                        </table> -->
                                        <!-- end table -->

                                    <!-- </div> -->
                                    <!--end table-responsive-->
                                <!-- </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- end row -->
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            @endsection

          