@extends('layouts.admin')
@section('title', 'Sub Admin')

@section('content')




                    <div class="row">
                    <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"> Sub Admin </h4>
                                        <br>
                                        <button type="button" class="btn btn-primary waves-effect waves-light mb-4" onclick="addnew()">Add New</button>

                                        <table id="Datatable" class="table table-responsive text-center table table-bordered dt-responsive  nowrap w-100" style="width:100% background-color:#17273a; color:#fff;">
                            <thead>
                                <tr>
                                <th>Sr No.</th>
                                <th>Photo</th>

                                    <th>Name</th>
                                    <th> Email</th>
                                    <th> Country</th>

                                    <th>Mobile</th> 
                                    <th>Action</th>                                   
                                  
                                </tr>
                            </thead>
                            <tbody>
                           
                             

                            </tbody>
                        </table> 
                        <x-datatables />
                                       
        
</div>
</div>
</div>
                     
                    </div>
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            <!-- addnew modal  -->
            <!-- <div class="modal fade"  tabindex="-1" aria-labelledby="exampleModalLabel"
  data-bs-backdrop="static" aria-hidden="true" id="formodal"> -->
  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" id="formmodal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Sub Admin</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="{{ route('admin.savesubadmin') }}" method="post" id="form" enctype="multipart/form-data">
                        @csrf
        <div class="modal-body p-4 bg-light">
        
       
          <div class="row" id="ban">
            <div class="col-lg">
              <label for="name">Name</label>
              <input type="hidden" name="id" id="id">

             <input type="text" name ="first_name" class="form-control" id="first_name">
                                <span class="text-danger errors " id="e-first_name"></span>  
                                      </div>
                                      
          
          </div>
          <div class="row" id="ban">
            <div class="col-lg">
              <label for="name">Last Name</label>

             <input type="text" name ="last_name" class="form-control" id="last_name">
                                <span class="text-danger errors " id="e-last_name"></span>  
                                      </div>
                                      
          
          </div>
          <div class="row">

          <div class="col-lg">
              <label for="name">Email</label>

             <input type="email" name ="email" class="form-control" id="email">
                                <span class="text-danger errors " id="e-email"></span>  
                                      </div>
                                      
          
          </div>
          <div class="col-md-12">

<div class="row">

    <label for="name">Mobile</label>
    <div class="col-md-2">
    <select class="form-control" name="countrycode" id="countrycode">
     @foreach($country as $c)
      <option value="{{$c->phonecode}}" <?php if($c->phonecode==44) {  ?>selected<?php } ?>>{{$c->phonecode}}</option>
     @endforeach
    </select>
    </div>

    <div class="col-md-8">
   <input type="number" name ="mobile" class="form-control" id="mobile">
   </div>
                      <span class="text-danger errors " id="e-mobile"></span>  
                            </div>
                            

</div>  
<div class="row">

<div class="col-lg">
    <label for="name">Address Line One</label>

   <input type="text" name ="address_line_one" class="form-control" id="address_line_one">
                      <span class="text-danger errors " id="e-address_line_one"></span>  
                            </div>
                            

</div>

<!-- <div class="row">

<div class="col-lg">
    <label for="name">Address Line Two</label>

   <input type="text" name ="address_line_two" class="form-control" id="address_line_two">
                      <span class="text-danger errors " id="e-address_line_two"></span>  
                            </div>
                            

</div> -->

<div class="row">

<div class="col-lg">
    <label for="name">Country</label>

   <input type="text" name ="country" class="form-control" id="country">
                      <span class="text-danger errors " id="e-country"></span>  
                            </div>
                            

</div>

<!-- <div class="row">

<div class="col-lg">
    <label for="name">State</label>

   <input type="text" name ="state" class="form-control" id="state">
                      <span class="text-danger errors " id="e-state"></span>  
                            </div>
                            

</div> -->
<div class="row">

<div class="col-lg">
    <label for="name">City</label>

   <input type="text" name ="city" class="form-control" id="city">
                      <span class="text-danger errors " id="e-city"></span>  
                            </div>
                            

</div>

<div class="row">

<div class="col-lg">
    <label for="name">Zip Code</label>

   <input type="number" name ="zipcode" class="form-control" id="zipcode">
                      <span class="text-danger errors " id="e-zipcode"></span>  
                            </div>
                            

</div>

<div class="row" id="ban">
            <div class="col-lg">
              <label for="name">Image</label>

             <input type="file" name="photo" class="form-control" id="photo">
                                <span class="text-danger errors " id="e-photo"></span>  
                                      </div>
                                      
          
          </div>
          <div class="row">
          <div class="col-lg">
                    <img id="preview-photo" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif"
                        alt="" style="max-height: 100px;">
                </div>
</div>

        
         
       
         <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" id="add_btn" class="btn btn-primary">Add </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- modal end  -->

<!-- full size image modal 
 -->
 
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true" id="imgmodal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Full Size Image</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                </div>
                <div class="modal-body">
                    <img src="" id="srclink" class="img-fluid" alt="" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
            @endsection
            @push('script')

            <script>
                  var imagefile = document.getElementById('photo');
                    var pimage = document.getElementById('preview-photo');

                 imagefile.onchange = evt => {
            var [file] = imagefile.files
            if (file) {
                pimage.src = URL.createObjectURL(file)
            }
        }

                function addnew(data=null){
                    $('.errors').hide();
            if (data == null) {
                $("#exampleModalLabel").text("Add  New Sub Admin");
                $("#add_btn").text("Add");
                $("#hid").val('');
                $("#form")[0].reset();
              

            } else {
                $("#exampleModalLabel").text("Update");
                $("#add_btn").text("Update");


                $(`#form input[name=id]`).val(data.id);

                $(`#first_name`).val(data.adminprofile.first_name);
                $(`#last_name`).val(data.adminprofile.last_name);

                $(`#email`).val(data.email);
                $(`#mobile`).val(data.mobile);
                $(`#address_line_one`).val(data.adminprofile.address_line_one);
                $(`#country`).val(data.adminprofile.country);
                $(`#city`).val(data.adminprofile.city);
                $(`#zipcode`).val(data.adminprofile.zipcode);
             pimage.src = "{{url('/')}}/storage/"+data.adminprofile.photo;











             
            }
            $("#formmodal").modal("toggle");
        }
        var loaded=0;

         function reload() {
            if (loaded) {
                $("#Datatable").DataTable().ajax.reload();
                $(".modal").modal('hide');
            } else {
                loaded = 1;
                $("#Datatable").DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.subadmin') }}",
                    columns: [{
                            data: 'id',
                            render: function(data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }                        },
                            {
                            data: 'photo',
                            name: 'photo'
                        },
                            {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'country',
                            name: 'country'
                        },
                        {
                            data: 'mobile',
                            name: 'mobile'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },
                    
                    ]
                });
               
            }

        }
                

                // form submit 

                $("#form").submit(function(e) {
            e.preventDefault();
            $('.errors').hide();
            var form = $(this);
            var formdata = new FormData(form[0]);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formdata,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function(r) {
                    $("#form")[0].reset();

                    
         
                    new Swal(
                  'Success',
                  'Subadmin login credentials has been  sent on email 👍',
                  'success'
                )
                    addnew();
                       reload();  


                  
                },
                error: function(r) {
                    $.each(r.responseJSON.errors, function(i, f) {
                        $('#e-' + i).show();
                        $('#e-' + i).text(f);
                    });

                }

            })

        });

                 
        window.onload = function() {
            reload();
        }
        function edit(id){
            $.get("{{ route('admin.subadmin') }}/?id=" + id, function(r) {
                addnew(r);

 console.log(r);


});
        }

        function deletedata(id){
            Swal.fire({
        icon: 'warning',
        text: 'Do you want to delte this record ?',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            $.get("{{ route('admin.subadmin') }}/?did=" + id, function(r) {
                reload();
});    
 }
    });
           
        }

       

       
      
      


</script>
            @endpush

          