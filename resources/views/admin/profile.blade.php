@extends('layouts.admin')
@section('title', 'Setting')

@section('content')




                    <div class="row">
                    <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"> Profile </h4>
                                        <form action="{{ route('admin.savesubadmin') }}" method="post" id="form" enctype="multipart/form-data">
                        @csrf                                      
                                        <br>
                                        <input type="hidden" name="id" id="id">

                                        <!-- start company setting div  -->
                                        <div class="col-md-12 companydiv" id="companydiv">
                                            <div class="row">
                                            <div class="col-md-12 bg-blue p-4">
                                            <p class="text-light" style="font-size:20px; font-weight:400;">Profile</p><div class="row">
                                          
                                     

                                          
                                            <div class="position-relative col-md-12 form-group mb-2">
                                            <div class="row">
                                         
                                     
                                            <div class="position-relative col-md-12 text-center form-group mb-4">

                                            <!-- <img class="rounded-circle shadow-4-strong" id="preview-applogo" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif"
                        alt="" style="max-height: 250px;"> -->
                        <input id="file" type="file" hidden="hidden" />
                        <img class="rounded-circle shadow-4-strong" alt="avatar2" id="preview-file" src="https://mdbcdn.b-cdn.net/img/new/avatars/1.webp" height="180" />
                        <span onclick="open_file()" class="text-center text-light pt-2" style="margin-bottom:30vh; ">Change Photo</span>


                                        </div>        
</div></div>           
                                        <div class="position-relative col-md-12 form-group mb-4">
                                            <div class="row">
                                            <div class="position-relative col-md-4 form-group mb-4">
                                            <label class="form-label text-light">First Name*</label>
                                            <input type="text" name="first_name" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" id="first_name" placeholder="Name" >
                                            <span class="text-danger errors " id="e-first_name"></span>  

                                        </div>
                                        <div class="position-relative col-md-4 form-group mb-4">
                                            <label class="form-label text-light">Last Name*</label>
                                            <input type="text" name="last_name" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" id="last_name" placeholder="Email" >
                                            <span class="text-danger errors " id="e-last_name"></span>  

                                        </div>
                                        <div class="position-relative col-md-4 form-group mb-4">
                                            <label class="form-label text-light">Email*</label>
                                            <input type="text" name="email" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" id="email" placeholder="Email" >
                                            <span class="text-danger errors " id="e-email"></span>  

                                        </div>
</div>
</div>

<div class="position-relative col-md-12 form-group mb-2">
                                            <div class="row">
                                            <div class="position-relative col-md-1 form-group mb-4">
                                            <label class="form-label text-light">Country*</label>
                                            <!-- <input type="text" name="country" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" placeholder="Country" id="country" > -->
                                            <select class="form-control" name="countrycode" id="countrycode">
     @foreach($country as $c)
      <option value="{{$c->phonecode}}">+{{$c->phonecode}}</option>
     @endforeach
    </select>
                                            <span class="text-danger errors " id="e-countrycode"></span>  

                                        </div>
                                        <div class="position-relative col-md-4 form-group mb-4">
                                            <label class="form-label text-light">Mobile*</label>
                                            <input type="text" name="state" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" placeholder="State" id="state" >
                                            <span class="text-danger errors " id="e-state"></span>  

                                        </div>
</div></div>

<div class="position-relative col-md-12 form-group mb-2">
                                            <div class="row">
                                            <div class="position-relative col-md-12 form-group mb-4">
                                            <label class="form-label text-light">Address Line 1*</label>
                                            <input type="text" name="address_line_one" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" placeholder="Address " id="address_line_one" >
                                            <span class="text-danger errors " id="e-address_line_one"></span>  

                                        </div>
</div></div>


<div class="position-relative col-md-12 form-group mb-2">
                                            <div class="row">
                                            <div class="position-relative col-md-12 form-group mb-4">
                                            <label class="form-label text-light">Address Line 2*</label>
                                            <input type="text" name="address_line_two" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" placeholder="Address " id="address_line_two" >
                                            <span class="text-danger errors " id="e-address_line_two"></span>  

                                        </div>
</div></div>

<div class="position-relative col-md-12 form-group mb-2">
                                            <div class="row">
                                            <div class="position-relative col-md-4 form-group mb-4">
                                            <label class="form-label text-light">Country*</label>
                                            <input type="number" name="country" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" placeholder="Zip Code" id="country">
                                            <span class="text-danger errors " id="e-country"></span>  

                                        </div>
                                        <div class="position-relative col-md-4 form-group mb-4">
                                            <label class="form-label text-light">State*</label>
                                            <input type="text" name="state" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" placeholder="State" id="state" >
                                            <span class="text-danger errors " id="e-state"></span>  

                                        </div>
                                        <div class="position-relative col-md-4 form-group mb-4">
                                            <label class="form-label text-light">City*</label>
                                            <input type="text" name="city" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" placeholder="city" id="city" >
                                            <span class="text-danger errors " id="e-city"></span>  

                                        </div>
</div></div>

<div class="position-relative col-md-12 form-group mb-2">
                                            <div class="row">
                                            <div class="position-relative col-md-6 form-group mb-4">
                                            <label class="form-label text-light">Zip Code*</label>
                                            <input type="number" name="zipcode" class="form-control label-field alt bg-dark-blue input-blue-border text-light" style="background-color:#17273a;" placeholder="Zip Code" id="zipcode">
                                            <span class="text-danger errors " id="e-zipcode"></span>  

                                        </div>
                                      
</div></div>
                                     
                                    </div>
                                </div>
                            </div>
                            <!-- end  -->



                                        <div class="col-md-12 form-group text-center mt-2 mb-2">
                                            <button class="btn btn-primary btn-lg" type="submit">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end social  -->
                        </div>
                                      
                                       
        
</div>
</div>
</form>
</div>
                     
                    </div>
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
        
 
            @endsection
            @push('script')

            <script>
                 var imagefile = document.getElementById('applogo');
                    var pimage = document.getElementById('preview-applogo');

                 imagefile.onchange = evt => {
            var [file] = imagefile.files
            if (file) {
                pimage.src = URL.createObjectURL(file)
            }
        }

        // fevicon
        var feviconfile = document.getElementById('fevicon');
                    var fimage = document.getElementById('preview-fevicon');

                 feviconfile.onchange = evt => {
            var [ffile] = feviconfile.files
            if (ffile) {
                fimage.src = URL.createObjectURL(ffile)
            }
        } 

        function open_file(){
      document.getElementById('file').click();
  }
                function addnew(data=null){
                    $('.errors').hide();
            if (data == null) {
                $("#exampleModalLabel").text("Add  New Genre");
                $("#add_btn").text("Add");
                $("#hid").val('');
                $("#form")[0].reset();
              

            } else {
                $("#add_btn").text("Update");


                $(`#form input[name=id]`).val(data.id);
                $(`#form input[name=name]`).val(data.name);
                $(`#form input[name=email]`).val(data.email);
                $(`#form input[name=phone]`).val(data.phone);
                $(`#form input[name=address]`).val(data.address);
                $(`#form input[name=country]`).val(data.country);
                $(`#form input[name=state]`).val(data.state);
                $(`#form input[name=city]`).val(data.city);
                $(`#form input[name=zipcode]`).val(data.zipcode);
                $(`#form input[name=facebook]`).val(data.facebook);
                $(`#form input[name=instagram]`).val(data.instagram);
                $(`#form input[name=twitter]`).val(data.twitter);
                $(`#form input[name=linkedin]`).val(data.linkedin);
                $(`#form input[name=youtube]`).val(data.youtube);
                pimage.src = "{{url('/')}}/storage/"+data.app_logo;
                fimage.src = "{{url('/')}}/storage/"+data.fevicon;










             
            }
        }

                

                // form submit 

                $("#form").submit(function(e) {
            e.preventDefault();
            $('.errors').hide();
            var form = $(this);
            var formdata = new FormData(form[0]);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formdata,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function(r) {
                    
         
                   
update();                  
                },
                error: function(r) {
                    $.each(r.responseJSON.errors, function(i, f) {
                        $('#e-' + i).show();
                        $('#e-' + i).text(f);
                    });

                }

            })

        });



                 
       
        function update(){
            var ok = 1;
            $.get("{{ route('admin.companysetting') }}/?id=" + ok, function(r) {
                addnew(r);

 console.log(r);


});
        }

        window.onload = function() {
            update();
        }
        function deletegenre(id){
            $.get("{{ route('admin.genre') }}/?did=" + id, function(r) {
                reload();

 console.log(r);


});
        }
        function openimage(src) {
            $("#imgmodal").modal("toggle");

            $("#srclink").attr("src", src);
        }
      
      


</script>
            @endpush

          