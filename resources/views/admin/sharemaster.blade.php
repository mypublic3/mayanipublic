@extends('layouts.admin')
@section('title', 'Share Content')

@section('content')




                    <div class="row">
                    <div class="col-12">
                                <div class="card" >
                                    <div class="card-body">
                                        <h4 class="card-title"> Share Content </h4>
                                        <br>
                                        <!-- <button type="button" class="btn btn-primary waves-effect waves-light mb-4" onclick="addnew()">Add New</button> -->

                                        <table id="Datatable" class="table table-responsive text-center table table-bordered dt-responsive  nowrap w-100" style="width:100%; background-color:#17273a; color:#fff;">
                            <thead>
                                <tr>
                                <th>Sr No.</th>

                                    <th> Content</th>

                                    <th>Action</th>                                   
                                  
                                </tr>
                            </thead>
                            <tbody>
                           
                             

                            </tbody>
                        </table> 
                        <x-datatables />
                                       
        
</div>
</div>
</div>
                     
                    </div>
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            <!-- addnew modal  -->
            <!-- <div class="modal fade"  tabindex="-1" aria-labelledby="exampleModalLabel"
  data-bs-backdrop="static" aria-hidden="true" id="formodal"> -->
  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" id="formmodal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Genre</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="{{ route('admin.saveshare') }}" method="post" id="form" enctype="multipart/form-data">
                        @csrf
        <div class="modal-body p-4 bg-light">
        
       
          <div class="row" id="ban">
            <div class="col-lg">
              <label for="name">Content</label>
              <input type="hidden" name="id" id="id">

             <textarea type="text" name ="content" class="form-control" id="content"></textarea>
                                <span class="text-danger errors " id="e-content"></span>  
                                      </div>
                                      
          
          </div>
        

      
         
       
         <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" id="add_btn" class="btn btn-primary">Add </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- modal end  -->

<!-- full size image modal 
 -->
 
            @endsection
            @push('script')

            <script>
         
                function addnew(data=null){
                    $('.errors').hide();
            if (data == null) {
                $("#exampleModalLabel").text("Add Share Content");
                $("#add_btn").text("Add");
                $("#hid").val('');
                $("#form")[0].reset();
              

            } else {
                $("#exampleModalLabel").text("Update");
                $("#add_btn").text("Update");


                $(`#form input[name=id]`).val(data.id);

                $(`#content`).text(data.content);






             
            }
            $("#formmodal").modal("toggle");
        }
        var loaded=0;

         function reload() {
            if (loaded) {
                $("#Datatable").DataTable().ajax.reload();
                $(".modal").modal('hide');
            } else {
                loaded = 1;
                $("#Datatable").DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.sharequote') }}",
                    columns: [{
                            data: 'id',
                            render: function(data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }                        },
                          
                        {
                            data: 'content',
                            name: 'content'
                        },
                       
                        {
                            data: 'action',
                            name: 'action'
                        },
                    
                    ]
                });
               
            }

        }
                

                // form submit 

                $("#form").submit(function(e) {
            e.preventDefault();
            $('.errors').hide();
            var form = $(this);
            var formdata = new FormData(form[0]);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formdata,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function(r) {
                    
         
                   
                    addnew();
reload();                   

                  
                },
                error: function(r) {
                    $.each(r.responseJSON.errors, function(i, f) {
                        $('#e-' + i).show();
                        $('#e-' + i).text(f);
                    });

                }

            })

        });

                 
        window.onload = function() {
            reload();
        }
        function edit(id){
            $.get("{{ route('admin.sharequote') }}/?id=" + id, function(r) {
                addnew(r);

 console.log(r);


});
        }

        function deletegenre(id){
            Swal.fire({
        icon: 'warning',
        text: 'Deleting the album will delete all the songs completely. You want to proceed?',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            $.get("{{ route('admin.genre') }}/?did=" + id, function(r) {
                reload();
});    
 }
    });
          
        }
        function openimage(src) {
            $("#imgmodal").modal("toggle");

            $("#srclink").attr("src", src);
        }
      
      


</script>
            @endpush

          