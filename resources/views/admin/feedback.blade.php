@extends('layouts.admin')
@section('title', 'Feedback')

@section('content')




                    <div class="row">
                    <div class="col-12">
                                <div class="card" >
                                    <div class="card-body">
                                        <h4 class="card-title"> Feedback </h4>
                                        <br>
                                     

                                        <table id="Datatable" class="table table-responsive text-center table table-bordered dt-responsive  nowrap w-100" style="width:100%; background-color:#17273a; color:#fff;">
                            <thead>
                                <tr>
                                <th>Sr No.</th>

                                    <th> Mobile</th>
                                    <th> Topic</th>
                                    <th> Feedback</th>


                                    <th>Action</th>                                   
                                  
                                </tr>
                            </thead>
                            <tbody>
                           
                             

                            </tbody>
                        </table> 
                        <x-datatables />
                                       
        
</div>
</div>
</div>
                     
                    </div>
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
          

<!-- full size image modal 
 -->
 
            @endsection
            @push('script')

            <script>
         
               
        var loaded=0;

         function reload() {
            if (loaded) {
                $("#Datatable").DataTable().ajax.reload();
                $(".modal").modal('hide');
            } else {
                loaded = 1;
                $("#Datatable").DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.feedback') }}",
                    columns: [{
                            data: 'id',
                            render: function(data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }                        },
                           
                        {
                            data: 'userprofile.mobile',
                            name: 'userprofile.mobile'
                        },
                        {
                            data: 'topic',
                            name: 'topic'
                        }, {
                            data: 'feedback',
                            name: 'feedback'
                        },
                       
                        {
                            data: 'action',
                            name: 'action'
                        },
                    
                    ]
                });
               
            }

        }
                

                // form submit 

               
                 
        window.onload = function() {
            reload();
        }
      

        function deletedata(id){
            Swal.fire({
        icon: 'warning',
        text: 'Are you sure you want to delete ? ',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            $.get("{{ route('admin.feedback') }}/?did=" + id, function(r) {
                reload();
});    
 }
    });
          
        }
      
      
      


</script>
            @endpush

          