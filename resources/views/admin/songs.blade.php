@extends('layouts.admin')
@section('title', 'Songs')

@section('content')




                    <div class="row">
                    <div class="col-12">
                                <div class="card" style="background-color:#17273a;">
                                    <div class="card-body">
                                        <h4 class="card-title"> Songs </h4>
                                        <br>
                                        <button type="button" class="btn btn-primary waves-effect waves-light mb-4" onclick="addnew()">Add New</button>

                                        <table id="Datatable" class="table table-responsive text-center table table-bordered dt-responsive  nowrap w-100" style="width:100%; background-color:#17273a; color:#fff;">
                            <thead>
                                <tr>
                                <th>Sr No.</th>

                                    <th>Photo</th>
                                    <th> Title</th>
                                    <th>Artist</th> 
                                    <th>Album</th> 
                                    <th>Stream Count</th> 

                                    <th>Download</th> 

                                    <th>Play</th> 

                                    <th>Added By</th> 


                                    <th>Action</th>                                   
                                  
                                </tr>
                            </thead>
                            <tbody>
                           
                             

                            </tbody>
                        </table> 
                        <x-datatables />
                                       
        
</div>
</div>
</div>
                     
                    </div>
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            <!-- addnew modal  -->
            <!-- <div class="modal fade"  tabindex="-1" aria-labelledby="exampleModalLabel"
  data-bs-backdrop="static" aria-hidden="true" id="formodal"> -->
  <div class="modal fade bs-example-modal-lg" tabindex="-1"  role="dialog" data-focus="false" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" id="formmodal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Song</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="{{ route('admin.savesong') }}" method="post" id="form" enctype="multipart/form-data">
                        @csrf
        <div class="modal-body p-4 bg-light">
        
        <div class="col-lg-12">

          <div class="row" >
            <div class="col-lg-6">
              <label for="name">Name</label>
              <input type="hidden" name="id" id="id">

             <input type="name" name ="name" class="form-control" id="name">
                                <span class="text-danger errors " id="e-name"></span>  
                                      </div>
                                      
          
            <div class="col-lg-6">
              <label for="name">Album</label>
              <select class="form-control" name="album" id="album"  style="width: 100%">
                  <option >** Select **</option>
                  @foreach($album as $a)
                  <option value="{{$a->id}}">{{$a->name}}</option>

                  @endforeach
              </select>

                                <span class="text-danger errors " id="e-album"></span>  
                                      </div></div></div>

                                      <div class="col-lg-12">

<div class="row" >
  <div class="col-lg-6">
              <label for="name">Artist</label>
              <select class="select2-multiple form-control" name="artist[]"  multiple="multiple"
                id="select2Multiple" style="width: 100%">
             
                  <!-- <option disabled>** Select **</option> -->
                  @foreach($artist as $ar)
                  <option value="{{$ar->id}}">{{$ar->name}}</option>

                  @endforeach
              </select>

                                <span class="text-danger errors " id="e-artist"></span>  
                                      </div>
            <div class="col-lg-6">
              <label for="name">Genre</label>
              <select class="form-control" name="genre" id="genre">
                  <option >** Select **</option>
                  @foreach($genre as $g)
                  <option value="{{$g->id}}">{{$g->genre}}</option>

                  @endforeach
              </select>

                                <span class="text-danger errors " id="e-genre"></span>  
                                      </div></div></div>

                                      <!-- date feild 
                                     -->
                                     <div class="col-lg-12">

          <div class="row" >
            <div class="col-lg-6">
    <label for="name">Year Of Song</label>

   <input type="date" name ="year_of_song" class="form-control" id="year_of_song">
                      <span class="text-danger errors " id="e-year_of_song"></span>  
                            </div>
                            


<div class="col-lg-6">
    <label for="name">Mastering</label>

   <input type="text" name ="mastering" class="form-control" id="mastering">
                      <span class="text-danger errors " id="e-mastering"></span>  
                            </div>
                            

</div></div>
<div class="col-lg-12">

<div class="row" >
  <div class="col-lg-6">
    <label for="name">Art Work</label>

   <input type="text" name ="artwork" class="form-control" id="artwork">
                      <span class="text-danger errors " id="e-artwork"></span>  
                            </div>
                            


<div class="col-lg-6">
    <label for="name">Mixing</label>

   <input type="text" name ="mixing" class="form-control" id="mixing">
                      <span class="text-danger errors " id="e-mixing"></span>  
                            </div>
                            
</div>
</div>

<div class="col-lg-12">

<div class="row" >
  <div class="col-lg-6">
    <label for="name">Writer</label>

   <input type="text" name ="writer" class="form-control" id="writer">
                      <span class="text-danger errors " id="e-writer"></span>  
                            </div>
                            


<div class="col-lg-6">
    <label for="name">Producer</label>

   <input type="text" name ="producer" class="form-control" id="producer">
                      <span class="text-danger errors " id="e-producer"></span>  
                            </div>
                            

</div></div>
<div class="col-lg-12">

<div class="row" >
  <div class="col-lg-6">
    <label for="name">Photograph</label>

   <input type="text" name ="photograph" class="form-control" id="photograph">
                      <span class="text-danger errors " id="e-photograph"></span>  
                            </div></div></div>
                            


                            <div class="col-lg-12">

<div class="row" >
          <label for="name">Lyrics</label>

   <textarea type="text" name ="lyrics" class="form-control" id="lyrics"></textarea>
                      <span class="text-danger errors " id="e-lyrics"></span>  
                            </div>
                            

</div>

                                       <div class="row">
            <div class="col-lg">
              <label for="name">Track</label>

             <input type="file" name="track" class="form-control" id="track">
                                <span class="text-danger errors " id="e-track"></span>  
                                      </div>
                                      
          
          </div>
         <div class="row" id="ban">
            <div class="col-lg">
              <label for="name">Image</label>

             <input type="file" name="image" class="form-control" id="image">
                                <span class="text-danger errors " id="e-image"></span>  
                                      </div>
                                      
          
          </div>
          <div class="row">
          <div class="col-lg">
                    <img id="preview-image" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif"
                        alt="" style="max-height: 100px;">
                </div>
</div>
       
         <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" id="add_btn" class="btn btn-primary">Add </button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- modal end  -->

<!-- full size image modal 
 -->
 
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true" id="imgmodal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Full Size Image</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                </div>
                <div class="modal-body">
                    <img src="" id="srclink" class="img-fluid" alt="" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
            @endsection
            @push('script')
           
    <link href="//cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>  -->

          


            <script>
            $(document).ready(function() {
            // Select2 Multiple
            // $('.select2').select2();
           

            $('.select2-multiple').select2({
                placeholder: "Select",
                allowClear: true
            });
            $('#album').select2({
        dropdownParent: $('#formmodal')
    });

        });   
         var imagefile = document.getElementById('image');
                    var pimage = document.getElementById('preview-image');

                 imagefile.onchange = evt => {
            var [file] = imagefile.files
            if (file) {
                pimage.src = URL.createObjectURL(file)
            }
        }
                function addnew(data=null){
                    $('.errors').hide();
            if (data == null) {
                $("#exampleModalLabel").text("Add  New Song");
                $("#add_btn").text("Add");
                $("#hid").val('');
                $("#form")[0].reset();
              

            } else {
                $("#exampleModalLabel").text("Update");
                $("#add_btn").text("Update");


                $(`#form input[name=id]`).val(data.id);
                 console.log(data.artists);
            
                $(`#name`).val(data.name);
                $(`#form select[name=album]`).val(data.albuminfo.id).selected = 'selected';
                $(`#form select[name=origin]`).val(data.id).selected = 'selected';
                // $(`#form select[name=artist]`).select2(JSON.parse(data.artists)).selected = 'selected';

                $(`#name`).val(data.name);
                $(`#genre`).val(data.genre);
                $(`#artwork`).val(data.art_work);
                $(`#mastering`).val(data.mastering);
                $(`#mixing`).val(data.mastering);
                $(`#writer`).val(data.writer);
                $(`#producer`).val(data.producer);
                $(`#photograph`).val(data.photographer);
                $(`#year_of_song`).val(data.year_of_song);

                $(`#lyrics`).text(data.lyrics);






                pimage.src = "{{url('/')}}/storage/"+data.image;




             
            }
            $("#formmodal").modal("toggle");
        }
        var loaded=0;

         function reload() {
            if (loaded) {
                $("#Datatable").DataTable().ajax.reload();
                $(".modal").modal('hide');
            } else {
                loaded = 1;
                $("#Datatable").DataTable({
                    processing: true,
                    serverSide: true,
                    searching: true,
                    lengthChange:false,
                    ajax: "{{ route('admin.songs') }}",
                    columns: [{
                            data: 'id',
                            render: function(data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }                        },
                            {
                            data: 'image',
                            name: 'image'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'artist',
                            name: 'artist'
                        },
                        {
                            data: 'albuminfo.name',
                            name: 'albuminfo.name'
                        },
                        {
                            data: 'stream_count',
                            name: 'stream_count'
                        },
                        {
                            data: 'download',
                            name: 'download'
                        },
                     

                        {
                            data: 'play',
                            name: 'play'
                        },
                        {
                            data: 'addedby',
                            name: 'addedby'
                        },
                        {
                            data: 'action',
                            name: 'action'
                        },
                    
                    ]
                });
               
            }

        }
                

                // form submit 
                $("#form").submit(function(e) {
            e.preventDefault();
            $('.errors').hide();
            $("#add_btn").prepend('<i class="fa fa-spinner fa-spin"></i>');
        $("#add_btn").attr("disabled", 'disabled');
        // $("#add_btn").text("Adding..");

            var form = $(this);
            var formdata = new FormData(form[0]);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formdata,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function(r) {
                    
                    $("#add_btn").find(".fa-spinner").remove();
                $("#add_btn").removeAttr("disabled");
                // $("#add_btn").text("Add");

                    addnew();
                   reload();                   

                  
                },
                error: function(r) {
                    $("#add_btn").find(".fa-spinner").remove();
                $("#add_btn").removeAttr("disabled");
                // $("#add_btn").text("Add");

                   

                    $.each(r.responseJSON.errors, function(i, f) {
                        $('#e-' + i).show();
                        $('#e-' + i).text(f);
                    });

                }

            })

        });

                 
        window.onload = function() {
           
            reload();
        }
        function edit(id){
            $.get("{{ route('admin.songs') }}/?id=" + id, function(r) {
                addnew(r);

 console.log(r);


});
        }

        function deletesong(id){
            Swal.fire({
        icon: 'warning',
        text: 'Do you want to delte this song ?',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            $.get("{{ route('admin.songs') }}/?did=" + id, function(r) {
                reload();
});    
 }
    });
           
        }
        function openimage(src) {
            $("#imgmodal").modal("toggle");

            $("#srclink").attr("src", src);
        }
      
      


</script>
            @endpush

          