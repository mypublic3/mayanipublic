@extends('layouts.admin')
@section('title', 'Users')

@section('content')




                    <div class="row">
                    <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title text-light"> Users </h4>
                                        <br>
                                        <!-- <button type="button" class="btn btn-primary waves-effect waves-light mb-4" onclick="addnew()">Add New</button> -->

                                        <table id="Datatable" class="table table-responsive text-center table table-bordered dt-responsive  nowrap w-100" style="width:100% background-color:#17273a; color:#fff;">
                            <thead>
                                <tr>
                                    <th>Sr No</th>
                                    <th> Name</th>
                                    <th>Mobile</th> 
                                    <th>Points</th> 
                                  


                                    <th>Action</th>                                   
                                  
                                </tr>
                            </thead>
                            <tbody style="background-color:#17273a;" >
                           
                             

                            </tbody>
                        </table> 
                        <x-datatables />
                                       
        
</div>
</div>
</div>
                     
                    </div>
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            <!-- addnew modal  -->
            <!-- <div class="modal fade"  tabindex="-1" aria-labelledby="exampleModalLabel"
  data-bs-backdrop="static" aria-hidden="true" id="formodal"> -->
  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" id="formmodal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">User Prefrences</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="{{ route('admin.saveusers') }}" method="post" id="form" enctype="multipart/form-data">
                        @csrf
        <div class="modal-body p-4 bg-light">
        
       
          <div class="row" id="ban">
            <div class="col-lg">
              <table  class="table table-striped table-bordered  nowrap w-100" style="width:100%">
            <tr>
                    <td>Artists</td>
                    <td id="artists"></td>

                </tr><tr>
                    <td>genres</td>
                    <td id="genre"></td>

                </tr>
              </table>
                                      </div>
                                      
          
          </div>
       
         <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- modal end  -->
            @endsection
            @push('script')

            <script>
               
        
        var loaded=0;

         function reload() {
            if (loaded) {
                $("#Datatable").DataTable().ajax.reload();
                $(".modal").modal('hide');
            } else {
                loaded = 1;
                $("#Datatable").DataTable({
                    processing: true,
                    serverSide: true,
                    // $('td', row).css('background-color', 'Red');

                    ajax: "{{ route('admin.users') }}",

                    columns: [{
                            data: 'id',
                            render: function(data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'mobile',
                            name: 'mobile'
                        },
                        {
                            data: 'allpoints',
                            name: 'allpoints'
                        },
                      
                       
                        {
                            data: 'action',
                            name: 'action'
                        },
                    
                    ],
            //       createdRow: function (row, data, dataIndex, cells) {
            //     if (data.stylesheet) {
            //         $(row).css('background-color','blue');

                   
            //     }
            // },
            createdRow: function(row, data, dataIndex) {
    if (data["0"] >= 0) {
      $(row).css("background-color", "Orange");
    //   $(row).addClass("warning");
    }
    else{
        $(row).css("background", "#17273a");


    }
  },
                });
               
            }

        }
                

                // form submit 

                $("#form").submit(function(e) {
            e.preventDefault();
            $('.errors').hide();
            var form = $(this);
            var formdata = new FormData(form[0]);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formdata,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function(r) {
                    
         
                   
                    addnew();
reload();                   

                  
                },
                error: function(r) {
                    $.each(r.responseJSON.errors, function(i, f) {
                        $('#e-' + i).show();
                        $('#e-' + i).text(f);
                    });

                }

            })

        });

                 
        window.onload = function() {
            reload();
        }
        function edit(id){
            $("#artists").html('');
            $("#genre").html('');
            $.get("{{ route('admin.users') }}/?id=" + id, function(r) {
                $("#formmodal").modal("toggle");

                for (const artist of r.artistsinfo){
    console.log(artist);
    $("#artists").append('&nbsp;&nbsp;'+artist.name+'');

}
for (const genre of r.genreinfo){
    console.log(genre);
    $("#genre").append('&nbsp;&nbsp;'+genre.genre+'');

}



 console.log(r);


});
        }

        function deleteuser(id){
            Swal.fire({
        icon: 'warning',
        text: 'Do you want to delte this record ?',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            $.get("{{ route('admin.users') }}/?did=" + id, function(r) {
                reload();
});    
 }
    });
        }
      
      


</script>




            @endpush

          