@extends('layouts.admin')
@section('title', 'Playlist')

@section('content')




                    <div class="row">
                    <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title"> Playlists </h4>
                                        <br>
                                        <button type="button" class="btn btn-primary waves-effect waves-light mb-4" onclick="addnew()">Add New</button>

                                        <table id="Datatable" class="table table-responsive text-center table table-bordered dt-responsive  nowrap w-100" style="width:100% background-color:#17273a; color:#fff;">
                            <thead>
                                <tr>
                                <th>Sr No.</th>

                                    <th>Photo</th>
                                    <th> Name</th>
                                    <th> Genre</th>

                                    <th>No Of Songs</th> 
                                    <th>Action</th>                                   
                                  
                                </tr>
                            </thead>
                            <tbody>
                           
                             

                            </tbody>
                        </table> 
                        <x-datatables />
                                       
        
</div>
</div>
</div>
                     
                    </div>
                 


                  
                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            <!-- addnew modal  -->
            <!-- <div class="modal fade"  tabindex="-1" aria-labelledby="exampleModalLabel"
  data-bs-backdrop="static" aria-hidden="true" id="formodal"> -->
  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true" id="formmodal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Playlist</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="{{ route('admin.saveplaylist') }}" method="post" id="form" enctype="multipart/form-data">
                        @csrf
        <div class="modal-body p-4 bg-light">
        
       
          <div class="row" id="ban">
            <div class="col-lg">
              <label for="name">Name</label>
              <input type="hidden" name="id" id="id">

             <input type="name" name ="name" class="form-control" id="name">
                                <span class="text-danger errors " id="e-name"></span>  
                                      </div>
                                      
          
          </div>
         
       

          <div class="row">
            <div class="col-lg">
              <label for="name">Image</label>

             <input type="file" name="image" class="form-control" id="image">
                                <span class="text-danger errors " id="e-image"></span>  
                                      </div>
                                      
          
          </div>
          <div class="row">
          <div class="col-lg">
                    <img id="preview-image" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif"
                        alt="" style="max-height: 250px;">
                </div>
</div>
       
         <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" id="add_btn" class="btn btn-primary">Add </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- modal end  -->

<!-- full size image modal 
 -->
 
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true" id="imgmodal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Full Size Image</h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

                </div>
                <div class="modal-body">
                    <img src="" id="srclink" class="img-fluid" alt="" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
            @endsection
            @push('script')

            <script>
                 var imagefile = document.getElementById('image');
                    var pimage = document.getElementById('preview-image');

                 imagefile.onchange = evt => {
            var [file] = imagefile.files
            if (file) {
                pimage.src = URL.createObjectURL(file)
            }
        }
                function addnew(data=null){
                    $('.errors').hide();
            if (data == null) {
                $("#exampleModalLabel").text("Add  New Playlist");
                $("#add_btn").text("Add");
                $("#hid").val('');
                $("#form")[0].reset();
              

            } else {
                $("#exampleModalLabel").text("Update");
                $("#add_btn").text("Update");


                $(`#form input[name=id]`).val(data.id);

                $(`#name`).val(data.name);

                pimage.src = "{{url('/')}}/storage/"+data.image;




             
            }
            $("#formmodal").modal("toggle");
        }
        var loaded=0;

         function reload() {
            if (loaded) {
                $("#Datatable").DataTable().ajax.reload();
                $(".modal").modal('hide');
            } else {
                loaded = 1;
                $("#Datatable").DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('admin.playlist') }}",
                    columns: [{
                            data: 'id',
                            render: function(data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }                        },
                            {
                            data: 'image',
                            name: 'image'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'genre',
                            name: 'genre'
                        },
                        {
                            data: 'playlistdetail_count',
                            name: 'playlistdetail_count'
                        },
                     
                        {
                            data: 'action',
                            name: 'action'
                        },
                    
                    ]
                });
               
            }

        }
                

                // form submit 

                $("#form").submit(function(e) {
            e.preventDefault();
            $('.errors').hide();
            var form = $(this);
            var formdata = new FormData(form[0]);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formdata,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success: function(r) {
                    
         
                   
                    addnew();
reload();                   

                  
                },
                error: function(r) {
                    $.each(r.responseJSON.errors, function(i, f) {
                        $('#e-' + i).show();
                        $('#e-' + i).text(f);
                    });

                }

            })

        });

                 
        window.onload = function() {
            reload();
        }
        function edit(id){
            $.get("{{ route('admin.playlist') }}/?id=" + id, function(r) {
                addnew(r);

 console.log(r);


});
        }

        function deleteplaylist(id){
            Swal.fire({
        icon: 'warning',
        text: 'Do you want to delte this record ?',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            $.get("{{ route('admin.playlist') }}/?did=" + id, function(r) {
                reload();
});    
 }
    });
          
        }

        function detail(id){
           window.location.href='{{url('/')}}/admin/playlistdetail?pid='+id;
        }

        function openimage(src) {
            $("#imgmodal").modal("toggle");

            $("#srclink").attr("src", src);
        }
      
      


</script>
            @endpush

          