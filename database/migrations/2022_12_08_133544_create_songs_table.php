<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;


return new class extends Migration
{
    use SoftDeletes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('album_id')->nullable();
            $table->foreign('album_id')->references('id')->on('albums')->onDelete('cascade');
            $table->string('image')->nullable();
            $table->string('track_file');
            $table->string('artists');
            $table->string('genre');
            $table->string('year_of_song');
            $table->string('art_work')->nullable();
            $table->string('mastering')->nullable();
            $table->string('mixing')->nullable();
            $table->string('writer')->nullable();
            $table->string('producer')->nullable();
            $table->string('photographer')->nullable();
            $table->text('lyrics')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
};
