<?php
namespace App\Helper;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

Trait Track
{   
    public $public_patht = "/public/uploadedTracks/";
    public $storage_patht = "/storage/uploadedTracks/";

    public function track( $file, $path ) : string
    {
       if ( $file ) {

           $extension       = $file->getClientOriginalExtension();
           $track_name       = Str::random(30).'.'.$extension;
           $url             = $file->storeAs($this->public_patht,$track_name);
           $public_patht     = public_path($this->storage_patht.$track_name);
           $url             = preg_replace( "/public/", "", $url );
        //    return $public_patht->save($public_patht) ? $url : '';
        return $url;
       }
    }
}