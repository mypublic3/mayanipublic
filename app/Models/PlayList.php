<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlayList extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];
    public function playlistdetail()
    {
        return   $this->belongsTo(PlaylistSong::class, 'id', 'playlist_id');
    }
    public function songinfo()
    {
        return   $this->belongsTo(Song::class, 'song_id', 'id');
    }

}
