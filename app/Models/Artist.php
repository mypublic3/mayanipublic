<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;


class Artist extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];
    
    public function countryinfo()
    {
        return   $this->belongsTo(Country::class, 'country_id', 'id');
    }
    public function song()
    {
        return   $this->belongsTo(Song::class, 'id')->whereJsonContains('artists','id');

    }
    public function ownerinfo(){
        return   $this->belongsTo(AdminProfile::class,'added_by','user_id');

    }
    public function followers(){
        return   $this->belongsTo(ArtistFollow::class,'id','artist_id');


    }
    // public function liked(){
    //     return $this->belongsToMany(Song::class,'like_songs','song_id', 'id')->withTimestamps();

    // }

    public function isfollow(){
        return $this->followers()->where('user_id',Auth::user()->id);
    }


}
