<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPrefrence extends Model
{
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    use HasFactory;
    use SoftDeletes;

    protected $casts = [
        'artist' => 'json',
        'genre' => 'json',
     ];
   
    protected $guarded = [];
    public function userdetail()
    {
        return   $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function artistsinfo()
    {
        return   $this->belongsToJson(Artist::class,'artist');
    }
    public function genreinfo()
    {
        return   $this->belongsToJson(Genre::class,'genre');
    }

}
