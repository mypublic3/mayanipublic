<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Song extends Model
{
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];
    protected $casts = [
        'artists' => 'json'
     ];

    public function albuminfo()
    {
        return   $this->belongsTo(Album::class, 'album_id', 'id');
    }

    public function artists()
    {
        return   $this->belongsToJson(Artist::class, 'artists');
    }

    public function liked(){
        return $this->belongsToMany(Song::class,'like_songs','song_id', 'id')->withTimestamps();

    }

    public function isliked(){
        return $this->liked()->where('user_id',Auth::user()->id);
    }

    public function ownerinfo(){
        return   $this->belongsTo(AdminProfile::class,'added_by','user_id');

    }

    



}
