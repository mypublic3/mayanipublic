<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class LikeSong extends Model
{
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    use HasFactory;

    protected $guarded = [];
    // protected $casts = [
    //     'songdetail.artists_ids' => 'json',
    //  ];

    public function songdetail()
    {
        return   $this->belongsTo(Song::class, 'song_id', 'id');
    }

  

    public function liked(){
        return $this->belongsToMany(Song::class,'like_songs','song_id', 'id')->withTimestamps();

    }

    public function isliked(){
        return $this->liked()->where('user_id',Auth::user()->id);
    }

}
