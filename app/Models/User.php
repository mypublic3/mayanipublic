<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,SoftDeletes;
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    // protected $fillable = [
    //     'name',
    //     'email',
    //     'mobile',
    //     'mobile_verified_at',
    //     'password',
    //     'country_id',
    //     'otp',
    //     'lastlogin',
    //     'ref_code',
    
    // ];
    protected $guarded = [];


    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        // 'prefrence.artist' => 'json',
        // 'prefrence.genre' => 'json'


    ];
 
    public function country()
    {
        return   $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function pointdetail()
    {
        return   $this->belongsTo(UserPoint::class, 'id', 'user_id');
    }

    public function adminprofile()
    {
        return   $this->belongsTo(AdminProfile::class, 'id', 'user_id');
    }
    public function profiledetail()
    {
        return   $this->belongsTo(UserProfile::class, 'id', 'user_id');
    }

    public function prefrence(){
        return $this->belongsTo(UserPrefrence::class,'id','user_id');

    }
    public function artistsinfo()
    {
        return   $this->belongsToJson(Artist::class,'prefrence.artist');
    }
    public function genreinfo()
    {
        return   $this->belongsToJson(Genre::class,'prefrence.genre');
    }

   
}
