<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Album extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];
    public function songinfo()
    {
        return   $this->belongsTo(Song::class, 'songs', 'id');
    }

    public function songs()
    {
        return   $this->belongsTo(AlbumSong::class, 'id', 'album_id');
    }
    public function ownerinfo(){
        return   $this->belongsTo(AdminProfile::class,'added_by','user_id');

    }

}
