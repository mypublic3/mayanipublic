<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LikePlaylist extends Model
{
    use HasFactory;

    protected $guarded = [];
}
