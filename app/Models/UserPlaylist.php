<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserPlaylist extends Model
{

    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

}
