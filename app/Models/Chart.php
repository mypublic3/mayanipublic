<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Chart extends Model
{
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];
    protected $casts = [
        'songs' => 'json',
        'songdetail.artists' => 'json'

     ];
    public function songdetail()
    {
        return   $this->belongsToJson(Song::class,'songs');
    }
    public function artists()
    {
        return   $this->belongsToJson(Artist::class, 'songdetail.artists');
    }
    public function liked(){
        return $this->belongsToMany(PlaylistSong::class,'like_songs','song_id', 'song_id')->withTimestamps();

    }

    public function isliked(){
        return $this->liked()->where('user_id',Auth::user()->id);
    }
}
