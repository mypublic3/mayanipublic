<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\File;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;


use App\Models\User;
use App\Models\Country;
use App\Models\Song;

use Auth;

use App\Models\Artist;
use Illuminate\Support\Facades\Hash;
use DataTables;

class ArtistController extends Controller
{
    use File;
    //
    
    public function users(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return User::where('id',$r->id)->get()->first();
            }
            if($r->cid!=null){
                $user = User::findOrFail($r->cid);

                if($user->status=='Active'){
                    $status='Inactive';
                }
                else{
                    $status='Active';
                }
                $user->status=$status;
                $user->save();             
                   return true;
            }
            if($r->did!=null){
                $user = User::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            $data = User::where('role','user')->get();

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('last_login',function($row){
                    $action=date('d-m-Y h:m:s',strtotime($row['lastlogin']));
                    return $action;
               })
                ->addColumn('created_at',function($row){
                     $action=date('d-m-Y h:m:s',strtotime($row['created_at']));
                     return $action;
                })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='edit({$row['id']})' class=' btn btn-success btn-sm'><i class='fa fa-edit'></i></button>
                    <button onclick='deleteuser({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['status','action','last_login','created_at'])
                ->make(true);
        }
        return view('admin.users');
    }

    
    public function artist(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return Artist::where('id',$r->id)->with('countryinfo')->get()->first();
            }
            if($r->cid!=null){
                $user = Artist::findOrFail($r->cid);

                if($user->status=='Active'){
                    $status='Inactive';
                }
                else{
                    $status='Active';
                }
                $user->status=$status;
                $user->save();             
                   return true;
            }
            if($r->did!=null){
                $user = Artist::findOrFail($r->did);
                
              
                $user->delete();
                return true;
            }
            if(Auth::user()->role=='admin'){
                $data = Artist::with('countryinfo')->withCount('song')->get();

            }
            else{
                $data = Artist::where('added_by',Auth::user()->id)->with('countryinfo')->withCount('song')->with('ownerinfo')->get();

            }

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                   
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row['image'])  . "\");'  src='" . asset('/storage'.$row['image']) . "' />";
                    return $actionBtn;
                })
                ->addColumn('followers',function($row){
                     $action="0";
                     return $action;
                })
                ->addColumn('addedby',function($row){
                    $action=$row->ownerinfo['first_name'];
                    return $action;
               })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='detail({$row['id']})' class=' btn btn-primary btn-sm'><i class='fa fa-eye'></i></button>
                    <button onclick='edit({$row['id']})' class=' btn btn-success btn-sm'><i class='fa fa-edit'></i></button>
                    <button onclick='deleteartist({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['image','followers','addedby','action'])
                ->make(true);
        }
        $country=Country::get();
        return view('admin.artist',compact('country'));
    }

    // save artist 
    public function saveartist(Request $r){
        $this->validate($r, [
            'name'=>'required',
            'origin'=>'required',
            'image' =>[Rule::requiredIf($r->id==null),'mimes:jpeg,png,jpg,gif,svg|max:1000'],
        ]);
        $artist=Artist::find($r->id);

        if($r->has('image')){  
            if($artist)
            if(is_file($artist->image))
            unlink(public_path('storage'.$artist->image));        
            $file   = $r->file('image');
            $path = 'artistImages';
            $url = $this->file($file,$path,300,400);
        }
        else{
            $url=$artist->image;
        }
        $save=Artist::updateOrcreate([
                
            'id'=>$r->id,
        ], [
         'name'=>$r->name,
        'image'=>$url,
        'country_id'=>$r->origin,      
        'added_by'=>$r->user()->id            
        
    ]);
    if($save)
    return true;
    return false;
    }

    public function artistdetail(Request $r){
        if ($r->ajax()) {
         
            if($r->did!=null){
                $user = Song::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
            $artist=Artist::find($r->aid);
         
            $data = Song::whereJsonContains('artists',$r->aid)->with('albuminfo')->get();

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                   
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row['image'])  . "\");'  src='" . asset('/storage'.$row['image']) . "' />";
                    return $actionBtn;
                })
               ->addColumn('play',function($row){
                $action="  <audio controls>
                <source src='" . asset('/storage'.$row['track_file']) . "' type='audio/mpeg'> 
                </audio>";
                return $action;
           })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='deletesong({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['image','play','action'])
                ->make(true);
        }
        $aid=$r->aid;
        return view('admin.artistdetail',compact('aid'));
    }



    
}
