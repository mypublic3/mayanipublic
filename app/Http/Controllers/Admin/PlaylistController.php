<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\File;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

use App\Models\User;
use App\Models\Country;
use App\Models\Artist;

use App\Models\Album;
use App\Models\Song;

use App\Models\AlbumSong;
use App\Models\Genre;

use App\Models\PlayList;
use App\Models\PlaylistSong;


use Illuminate\Support\Facades\Hash;
use DataTables;
use App\Helper\Notification;

class PlaylistController extends Controller
{
     use File;
     use Notification;
    //
    
    public function playlist(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return playlist::where('id',$r->id)->get()->first();
            }
            if($r->cid!=null){
                $user = playlist::findOrFail($r->cid);

                if($user->status=='Active'){
                    $status='Inactive';
                }
                else{
                    $status='Active';
                }
                $user->status=$status;
                $user->save();             
                   return true;
            }
            if($r->did!=null){
                $user = playlist::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            $data = playlist::withCount('playlistdetail')->orderBy('created_at','desc')->get();

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                   
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row['image'])  . "\");'  src='" . asset('/storage'.$row['image']) . "' />";
                    return $actionBtn;
                })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='detail({$row['id']})' class=' btn btn-primary btn-sm'><i class='fa fa-eye'></i></button>
                    <button onclick='edit({$row['id']})' class=' btn btn-success btn-sm'><i class='fa fa-edit'></i></button>
                    <button onclick='deleteplaylist({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['image','action'])
                ->make(true);
        }
        $genre=Genre::get();
        return view('admin.playlist',compact('genre'));
    }
    

    // save playlist 
    public function saveplaylist(Request $r){
        $this->validate($r, [
            'name'=>'required',
            'image' =>[Rule::requiredIf($r->id==null),'mimes:jpeg,png,jpg,gif,svg|max:1000'],
        ]);
        $playlist=playlist::find($r->id);

        if($r->has('image')){  
            if($playlist)
            if(is_file($playlist->image))
            unlink(public_path('storage'.$playlist->image));       
            $file   = $r->file('image');
            $path = 'playlistImages';
            $url = $this->file($file,$path,300,400);
        }
        else{
            $url=$playlist->image;
        }
        $save=PlayList::updateOrcreate([
                
            'id'=>$r->id,
        ], [
         'name'=>$r->name,
        'image'=>$url
        ]);
        $title="New playlist added";
        $body=''.$r->name.' playlist added for you';
        if($save)
        $this->notifications($title,$body);
    return true;
    return false;
    }

    public function playlistdetail(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return playlistSong::where('id',$r->id)->get()->first();
            }
            
            if($r->did!=null){
                $user = PlaylistSong::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            $data = playlistSong::where('playlist_id',$r->pid)->with('songdetail')->withCount('songdetail')->get();

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                   
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row->songdetail['image'])  . "\");'  src='" . asset('/storage'.$row->songdetail['image']) . "' />";
                    return $actionBtn;
                })
                ->addColumn('artists',function($row){
                    $action=json_decode($row->songdetail['artists']);
                    return $action;
               })

               ->addColumn('play',function($row){
                $action="  <audio controls>
                <source src='" . asset('/storage'.$row->songdetail['track_file']) . "' type='audio/mpeg'> 
                </audio>";
                return $action;
           })
              
               
                ->addColumn('action',function($row){
                    $action = "
             
                  
                    <button onclick='deleteplaylistsong({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['image','play','action'])
                ->make(true);
        }
        $song=Song::get();
        $pid=$r->pid;
        $plsongs = PlaylistSong::where('playlist_id',$r->pid)->with('songdetail')->withCount('songdetail')->get();


        return view('admin.playlistdetail',compact('song','pid','plsongs'));
    }
     // drag n drop update function 
     public function updateorderps(Request $request)
     {
         $tasks = PlaylistSong::get();
 
         foreach ($tasks as $task) {
             $task->timestamps = false; // To disable update_at field updation
             $id = $task->id;
 
             foreach ($request->order as $order) {
                 if ($order['id'] == $id) {
                     $task->update(['order' => $order['position']]);
                 }
             }
         }
         
         return response('Update Successfully.', 200);
     }

     // save playlist 
     public function savesongtoplaylist(Request $r){
        $this->validate($r, [
            'song'=>'required|exists:songs,id',
        ]);
        $playlist=playlistSong::find($r->id);

      
        $save=PlaylistSong::updateOrcreate([
              
         'playlist_id'=>$r->playlist_id,
        'song_id'=>$r->song,
    ]);
    if($save)
    return true;
    return false;
    }



}
