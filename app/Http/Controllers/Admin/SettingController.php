<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helper\File;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;


use App\Models\User;
use App\Models\CmsSetting;
use App\Models\CompanySetting;


use App\Models\Country;

use App\Models\Genre;
use App\Models\Feedback;
use App\Models\ShareQuote;

use DataTables;


class SettingController extends Controller
{
    use File;
    //
    public function companysetting(Request $r){
        if($r->ajax()){
            if($r->id!=null){
                return CompanySetting::where('user_id',$r->user()->id)->get()->first();
           }
        }
      return view('admin.companysetting');

    }

    public function savesetting(Request $r){
        $this->validate($r, [
            'name'=>'required',
            'applogo' =>[Rule::requiredIf($r->id==null),'mimes:jpeg,png,jpg,gif,svg|max:1000'],
            'fevicon' =>[Rule::requiredIf($r->id==null),'mimes:jpeg,png,jpg,gif,svg|max:1000'],

        ]);
        $setting=CompanySetting::find($r->id);

        if($r->has('applogo')){  
            if($setting)
            if(is_file($setting->app_logo))
            unlink(public_path('storage'.$setting->app_logo));       
            $afile   = $r->file('applogo');
            $apath = 'uploadedImages';
            $aurl = $this->file($afile,$apath,300,400);
        }
        else{
            $aurl=$setting->app_logo;
        }
        if($r->has('fevicon')){  
            if($setting)
            if(is_file($setting->fevicon))
            unlink(public_path('storage'.$setting->fevicon));       
            $ffile   = $r->file('fevicon');
            $fpath = 'playlistImages';
            $furl = $this->file($ffile,$fpath,300,400);
        }
        else{
            $furl=$setting->fevicon;
        }
        $save=CompanySetting::updateOrcreate([
                
            'user_id'=>$r->user()->id,
        ], [
         'name'=>$r->name,
        'email'=>$r->email,
        'phone'=>$r->phone,
        'country_code'=>$r->country_code,
        'address'=>$r->address,
        'country_code'=>'+91',
        'country'=>$r->country,
        'state'=>$r->state,
        'city'=>$r->city,
        'zipcode'=>$r->zipcode,
        'app_logo'=>$aurl,
        'fevicon'=>$furl,
        'facebook'=>$r->facebook,
        'linkedin'=>$r->linkedin,
        'instagram'=>$r->instagram,
        'twitter'=>$r->twitter,
        'youtube'=>$r->youtube             
    ]);
    if($save)
    return true;
    return false;
    }

    public function cmssetting(Request $r){
        if($r->ajax()){
            if($r->id!=null){
                return CmsSetting::where('user_id',$r->user()->id)->get()->first();
           }
        }
        return view('admin.cmssetting');
    }

    
    public function savecmssetting(Request $r){
        // $this->validate($r, [
        //     'aboutus'=>'required',
          
        // ]);
      
   
        $save=CmsSetting::updateOrcreate([
            'user_id'=>$r->user()->id,
        ], [
         'aboutus'=>$r->aboutus,
        'privacy'=>$r->privacy,
        'terms_conitions'=>$r->terms_conitions,
        'legel_disclaimer'=>$r->legel_disclaimer,
        'how_to_pay'=>$r->how_to_pay,
                   
    ]);
    if($save)
    return true;
    return false;
    }

    public function feedback(Request $r){
        if ($r->ajax()) {
           
       
            if($r->did!=null){
                $user = Feedback::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            $data = Feedback::with('userprofile')->get();

            return  Datatables::of($data)
                ->addIndexColumn()

              
               
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='deletedata({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.feedback');
    
    }
    
    public function sharequote(Request $r){
        if ($r->ajax()) {
            if($r->id!=null){
                return ShareQuote::where('id',$r->id)->get()->first();
            }
           
       
            if($r->did!=null){
                $user = ShareQuote::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            $data = ShareQuote::get();

            return  Datatables::of($data)
                ->addIndexColumn()

              
               
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='edit({$row['id']})' class=' btn btn-success btn-sm'><i class='fa fa-edit'></i></button>";
                    return $action;
               })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.sharemaster');
    
    }
    public function saveshare(Request $r){
        $this->validate($r, [
            'content'=>'required',
          
        ]);
      
   
        $save=ShareQuote::updateOrcreate([
            'id'=>$r->id,
        ], [
         'content'=>$r->content,
       
                   
    ]);
    if($save)
    return true;
    return false;
    }


}
