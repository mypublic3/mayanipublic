<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\File;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;


use App\Models\User;
use App\Models\Country;
use Auth;

use App\Models\Genre;
use Illuminate\Support\Facades\Hash;




use DataTables;
class GenreController extends Controller
{
    //
    
    use File;
    //
    
    public function genre(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return Genre::where('id',$r->id)->get()->first();
            }
            if($r->cid!=null){
                $user = Genre::findOrFail($r->cid);

                if($user->status=='Active'){
                    $status='Inactive';
                }
                else{
                    $status='Active';
                }
                $user->status=$status;
                $user->save();             
                   return true;
            }
            if($r->did!=null){
                $user = Genre::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            if(Auth::user()->role=='admin'){
                $data = Genre::get();

            }
            else{
                $data = Genre::where('added_by',Auth::user()->id)->with('ownerinfo')->get();

            }

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('addedby',function($row){
                    $action=$row->ownerinfo['first_name'];
                    return $action;
               })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='edit({$row['id']})' class=' btn btn-success btn-sm'><i class='fa fa-edit'></i></button>
                    <button onclick='deletegenre({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['addedby','action'])
                ->make(true);
        }
        return view('admin.genre');
    }

    // save Genre 
    public function savegenre(Request $r){
        $this->validate($r, [
            'genre'=>'required',
        ]);
        $Genre=Genre::find($r->id);

        $save=Genre::updateOrcreate([
                
            'id'=>$r->id,
        ], [
         'genre'=>$r->genre, 
         'added_by'=>$r->user()->id            
    ]);
    if($save)
    return true;
    return false;
    }
}
