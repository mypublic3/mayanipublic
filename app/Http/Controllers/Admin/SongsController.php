<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Artist;
use App\Models\Album;
use App\Models\AlbumSong;
use App\Models\PlaylistSong;
use App\Models\Chart;
use App\Models\UserPrefrence;


use Auth;
use App\Models\Genre;
use App\Models\Song;
use App\Helper\File;
use App\Helper\Track;
use DataTables;





class SongsController extends Controller
{
    use File;
     use Track;
    //
    public function dashboard(Request $r){
        if($r->user()->role=='admin'){
            $songs=Song::count();
            $artists=Artist::count();
            $users=User::where('role','user')->count();
        }
        else{
            $songs=Song::where('added_by',$r->user()->id)->count();
            $artists=Artist::where('added_by',$r->user()->id)->count();
            $users=User::where('role','user')->count();
        }
     

        return view('admin.dashboard',compact('songs','artists','users'));
    }

    public function users(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                //  return User::where('id',$r->id)->with('pointdetail','prefrence','artistsinfo','genreinfo')->get()->first();
                 return UserPrefrence::select('id','artist','genre')->where('user_id',$r->id)->with('artistsinfo','genreinfo')->get()->first();
            }
            if($r->cid!=null){
                $user = User::findOrFail($r->cid);

                if($user->status=='Active'){
                    $status='Inactive';
                }
                else{
                    $status='Active';
                }
                $user->status=$status;
                $user->save();             
                   return true;
            }
            if($r->did!=null){
                $user = User::findOrFail($r->did);
              
                $user->forcedelete();
                return true;
            }
         
            $data = User::where('role','user')->with('pointdetail','prefrence','artistsinfo','genreinfo')->get();
            
            return  Datatables::of($data)
                ->addIndexColumn()
               ->addColumn('allpoints',function($row){
                $point=$row->pointdetail;
                if($point==NULL)
                {
                $fpoint=0;
                return $fpoint;

                }
                return $row->pointdetail['points'];
           })
           ->addColumn('artists',function($row){
            $ainfo=$row->artistinfo;
            if($ainfo==NULL)
            {
            $ainfo=0;
            return $ainfo;

            }
            return $row->artistinfo;
       }) ->addColumn('genre',function($row){
        $ginfo=$row->genreinfo;
        if($ginfo==NULL)
        {
        $ginfo=0;
        return $ginfo;

        }
        return $row->genreinfo;
   })
           ->addColumn('playpoints',function($row){
            $ppoint=$row->pointdetail;
            if($ppoint==NULL)
            {
            $fppoint=0;
            return $fppoint;
            }
            return $row->pointdetail['play_point'];
       })->addColumn('refpoints',function($row){
        $roint=$row->pointdetail;
        if($roint==NULL)
        {
        $frpoint=0;
        return $frpoint;
        }
        return $row->pointdetail['referal_point'];
   })
   ->addColumn('last_login',function($row){
    $action=date('d-m-Y h:m:s',strtotime($row['lastlogin']));
    return $action;
})
                ->addColumn('created_at',function($row){
                     $action=date('d-m-Y h:m:s',strtotime($row['created_at']));
                     return $action;
                })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='edit({$row['id']})' class=' btn btn-success btn-sm'>Prefrence</button>
                    <button onclick='deleteuser({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['allpoints','playpoints','refpoints','artists','genre','status','action','last_login','created_at'])
                ->make(true);
        }
        return view('admin.users');
    }

   
    public function songs(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return Song::where('id',$r->id)->with('albuminfo')->get()->first();
            }
         
            if($r->did!=null){
                $song = Song::findOrFail($r->did);
            //    return $did=array($r->did);
                  $chart=Chart::where('songs','!=',NULL)->get();
                foreach($chart as $v=>$c)
                {
                    if(in_array($r->did,$c->songs)){
                        $collection = collect($c->songs);
 
                        $diff = $collection->diff([$r->did]);
                        // $diff->all();
                        $c->songs=$diff;
                        $c->save();
                    }
                }
                $albumsong=AlbumSong::where('song_id',$r->did)->delete();
                $playlistsong=PlaylistSong::where('song_id',$r->did)->delete();
               
              
                $song->delete();
                return true;
            }
         
            if(Auth::user()->role=='admin'){
        $data =Song::with('albuminfo')->with('ownerinfo')->orderBy('created_at', 'asc')->get();

            }
            else{
                $data =Song::where('added_by',$r->user()->id)->with('albuminfo')->with('ownerinfo')->orderBy('created_at', 'asc')->get();

            }

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                    
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row['image'])  . "\");'  src='" . asset('/storage'.$row['image']) . "' />";
                    return $actionBtn;
                })
                ->addColumn('artist',function($row){
                    $action=json_encode($row['artists']);
                    $action=json_decode($action);
                    return $action;
               })
               
                ->addColumn('download',function($row){
                    $action="0";
                    return $action;
               })
               ->addColumn('play',function($row){
                $action="  <audio controls>
                <source src='" . asset('/storage'.$row['track_file']) . "' type='audio/mpeg'> 
                </audio>";
                return $action;
           })
           ->addColumn('addedby',function($row){
            $action=$row->ownerinfo['first_name'];
            return $action;
       })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='view({$row['id']})' class=' btn btn-primary btn-sm'><i class='fa fa-eye'></i></button>
                    <button onclick='edit({$row['id']})' class=' btn btn-success btn-sm'><i class='fa fa-edit'></i></button>
                    <button onclick='deletesong({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['image','download','play','addedby','action'])
                ->make(true);
        }
        $artist=Artist::get();
        $album=Album::get();
        $genre=Genre::get();

        return view('admin.songs',compact('artist','album','genre'));
    }

    // save artist 
    public function savesong(Request $r){
        $this->validate($r, [
            'name'=>'required',
            'artist'=>'required',

            'image' =>[Rule::requiredIf($r->id==null),'mimes:jpeg,png,jpg,gif,svg|max:2000'],
            'track' =>[Rule::requiredIf($r->id==null),'mimes:mp3'],

        ]);
        $song=Song::find($r->id);

        if($r->has('image')){  
            if($song)

            if(is_file($song->image))
            unlink(public_path('storage'.$song->image));        
            $filei   = $r->file('image');
            $pathi = 'artistImages';
            $urli = $this->file($filei,$pathi,300,400);
        }
        else{
            $urli=$song->image;
        }
        if($r->has('track')){   
            if($song)

            if(is_file($song->track_file))
            unlink(public_path('storage'.$song->track_file));         
            $file   = $r->file('track');
            $path = 'trackFile';
            $url = $this->track($file,$path);
        }
        else{
            $url=$song->track_file;
        }
        // $artist=json_encode($r->artist);
        // $artist=json_decode($artist);
        // return $r->artist;
        // $intArray = array_map(
        //     function($value) { return (int)$value; },
        //     $r->artist
        // );
        
        $getartistname=Artist::whereIn('id',$r->artist)->pluck('name');
        $intArray=Artist::whereIn('id',$r->artist)->pluck('id');

        $save=Song::updateOrcreate([
                
            'id'=>$r->id,
        ], [
         'name'=>$r->name,
         'album_id'=>$r->album,
        'image'=>$urli,
        'track_file'=>$url,
        'artists'=>$getartistname,
        'artists_ids'=>$intArray,
        'genre'=>$r->genre,
        'year_of_song'=>$r->year_of_song,
        'art_work'=>$r->artwork,
        'mastering'=>$r->mastering,
        'mixing'=>$r->mixing,
        'writer'=>$r->writer,
        'producer'=>$r->producer,
        'photographer'=>$r->photograph,
        'lyrics'=>$r->lyrics,
        'added_by'=>$r->user()->id            

    ]);
    AlbumSong::updateOrcreate(['album_id'=>$r->album,'song_id'=>$save->id],['order'=>0]);
    if($save)
    return true;
    return false;
    }

}
