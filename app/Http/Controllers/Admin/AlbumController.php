<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

use App\Helper\File;
use App\Models\User;
use App\Models\Country;
use App\Models\Artist;
use App\Models\Song;
use App\Models\Chart;
use App\Models\PlaylistSong;

use App\Models\Album;
use App\Models\AlbumSong;
use App\Helper\Notification;


use Illuminate\Support\Facades\Hash;



use Auth;
use DataTables;

class AlbumController extends Controller
{
    use File;
    use Notification;
    //
    
    public function album(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return Album::where('id',$r->id)->get()->first();
            }
            if($r->cid!=null){
                $user = Album::findOrFail($r->cid);

                if($user->status=='Active'){
                    $status='Inactive';
                }
                else{
                    $status='Active';
                }
                $user->status=$status;
                $user->save();             
                   return true;
            }
            if($r->did!=null){
                $album = Album::findOrFail($r->did);
                $getsongs=Song::where('album_id',$r->did)->get();
                $chart=Chart::where('songs','!=',NULL)->get();
                foreach($getsongs as $gs=>$g){
                foreach($chart as $v=>$c)
                {
                    if(in_array($g->id,$c->songs)){
                        $collection = collect($c->songs);
 
                        $diff = $collection->diff([$g->id]);
                        // $diff->all();
                        $c->songs=$diff;
                        $c->save();
                    }
                }
                $albumsong=AlbumSong::where('song_id',$g->id)->delete();
                $playlistsong=PlaylistSong::where('song_id',$g->id)->forceDelete();
            }
              
               
            $getsongs=Song::where('album_id',$r->did)->delete();
                $album->delete();
                return true;
            }
            if(Auth::user()->role=='admin'){
                $data = Album::withCount('songs')->orderBy('created_at','desc')->get();

            }
            else{
                $data = Album::where('added_by',Auth::user()->id)->with('ownerinfo')->withCount('songs')->orderBy('created_at','desc')->get();

            }

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                   
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row['image'])  . "\");'  src='" . asset('/storage'.$row['image']) . "' />";
                    return $actionBtn;
                })
                ->addColumn('followers',function($row){
                     $action="0";
                     return $action;
                })
                ->addColumn('albumyear',function($row){
                    $date=$row['date'];
                    $year=date('Y', strtotime($date));
                    // $year=new Carbon
                    return $year;
               })
                ->addColumn('addedby',function($row){
                    $action=$row->ownerinfo['first_name'];
                    return $action;
               })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='detail({$row['id']})' class=' btn btn-primary btn-sm'><i class='fa fa-eye'></i></button>
                    <button onclick='edit({$row['id']})' class=' btn btn-success btn-sm'><i class='fa fa-edit'></i></button>
                    <button onclick='deletealbum({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['image','followers','albumyear','addedby','action'])
                ->make(true);
        }
        return view('admin.album');
    }

    // save Album 
    public function saveAlbum(Request $r){
        $this->validate($r, [
            'name'=>'required',
            'date'=>'required',
            'image' =>[Rule::requiredIf($r->id==null),'mimes:jpeg,png,jpg,gif,svg|max:1000'],
        ]);
        $Album=Album::find($r->id);

        if($r->has('image')){  
            if($Album)
            if(is_file($Album->image))
            unlink(public_path('storage'.$Album->image));  
            // unlink(storage_path('app/public'.$Album->image));  

            // Storage::delete($Album->image);   
            $file   = $r->file('image');
            $path = 'albumImages';
            $url = $this->file($file,$path,300,400);
        }
        else{
            $url=$Album->image;
        }
        $save=Album::updateOrcreate([
                
            'id'=>$r->id,
        ], [
         'name'=>$r->name,
        'image'=>$url,
        'date'=>$r->date,      
        'added_by'=>$r->user()->id            
        
    ]);
    $title="New album added";
    $body=''.$r->name.' album added for you';
    if($save)
    $this->notifications($title,$body);

    return true;
    return false;
    }

    public function albumdetail(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return AlbumSong::where('id',$r->id)->get()->first();
            }
            
            if($r->did!=null){
                $user = AlbumSong::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            $data = Song::where('album_id',$r->aid)->with('artist')->get();

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                   
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row->songdetail['image'])  . "\");'  src='" . asset('/storage'.$row->songdetail['image']) . "' />";
                    return $actionBtn;
                })
                ->addColumn('artists',function($row){
                    $action=json_decode($row->songdetail['artists']);
                    return $action;
               })

               ->addColumn('play',function($row){
                $action="  <audio controls>
                <source src='" . asset('/storage'.$row->songdetail['track_file']) . "' type='audio/mpeg'> 
                </audio>";
                return $action;
           })
              
               
                ->addColumn('action',function($row){
                    $action = "
             
                  
                    <button onclick='deletealbumsong({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['image','play','action'])
                ->make(true);
        }
        $song=Song::get();
        $aid=$r->aid;
        $albumsong=AlbumSong::where('album_id',$r->aid)->with('songdetail')->withCount('songdetail')->get();

        return view('admin.albumdetail',compact('song','aid','albumsong'));
    }
     // drag n drop update function 
     public function updateorderas(Request $request)
     {
         $tasks = AlbumSong::get();
 
         foreach ($tasks as $task) {
             $task->timestamps = false; // To disable update_at field updation
             $id = $task->id;
 
             foreach ($request->order as $order) {
                 if ($order['id'] == $id) {
                     $task->update(['order' => $order['position']]);
                 }
             }
         }
         
         return response('Update Successfully.', 200);
     }
 

     // save Album 
     public function savesongtoalbum(Request $r){
        $this->validate($r, [
            'song'=>'required|exists:songs,id',
        ]);
        $Album=AlbumSong::find($r->id);

      
        $save=AlbumSong::updateOrcreate([
                
            'album_id'=>$r->albumid,
            'song_id'=>$r->song,
                
        
    ]);
    if($save)
    return true;
    return false;
    }

}
