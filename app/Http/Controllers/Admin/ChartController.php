<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\File;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

use App\Models\User;
use App\Models\Country;
use App\Models\Artist;

use App\Models\Album;
use App\Models\Song;

use App\Models\AlbumSong;
use App\Models\Genre;

use App\Models\PlayList;
use App\Models\PlaylistSong;
use App\Models\Chart;
use App\Models\Subscription;


use Illuminate\Support\Facades\Hash;
use DataTables;
class ChartController extends Controller
{
    //
    use File;

    public function chart(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return Chart::where('id',$r->id)->get()->first();
            }
       
            if($r->did!=null){
                $user = Chart::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            $data = Chart::get();

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                   
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row['image'])  . "\");'  src='" . asset('/storage'.$row['image']) . "' />";
                    return $actionBtn;
                })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='view({$row['id']})' class=' btn btn-primary btn-sm'><i class='fa fa-eye'></i></button>
                    <button onclick='deletechart({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
                ->rawColumns(['image','action'])
                ->make(true);
        }
        $genre=Genre::get();

        return view('admin.chart',compact('genre'));
    }

    // save Genre 
    public function savechart(Request $r){
        $this->validate($r, [
            'name'=>'required',
            'genre'=>'required',
            'no_of_songs'=>'required|numeric|min:5',
            'image'=>[Rule::requiredIf($r->id==null),'mimes:jpeg,png,jpg,gif,svg|max:1000']

        ]);
        $chart=Chart::find($r->id);

        if($r->has('image')){  
            if($chart)
            if(is_file($chart->image))
            unlink(public_path('storage'.$chart->image));        
            $filei   = $r->file('image');
            $pathi = 'chartImages';
            $urli = $this->file($filei,$pathi,300,400);
        }
        else{
            $urli=$chart->image;
        }
        $getsongs=Song::where('genre',$r->genre)->take($r->no_of_songs)->pluck('id');
        // $jsonsongs=json_encode($getsongs);

        $save=Chart::updateOrcreate([
                
            'id'=>$r->id,
        ], [
            'name'=>$r->name,
         'genre'=>$r->genre, 
         'no_of_songs'=>$r->no_of_songs,
         'image'=>$urli,
         'songs'=>$getsongs      
    ]);
    if($save)
    return true;
    return false;
    }

    // subscripion 
    
    public function subscription(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return Subscription::where('id',$r->id)->get()->first();
            }
       
            if($r->did!=null){
                $user = Subscription::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            $data = Subscription::get();

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('image', function ($row) {
                   
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row['image'])  . "\");'  src='" . asset('/storage'.$row['image']) . "' />";
                    return $actionBtn;
                })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='view({$row['id']})' class=' btn btn-primary btn-sm'><i class='fa fa-eye'></i></button>
                    <button onclick='edit({$row['id']})' class=' btn btn-success btn-sm'><i class='fa fa-edit'></i></button>
                    <button onclick='deletechart({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
               
            
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.subscription');
    }

    // save Genre 
    public function savesubscription(Request $r){
        $this->validate($r, [
            'name'=>'required',
            'plan_type'=>'required',
            'price'=>'required|numeric',
            'point'=>'required|numeric',
              'description'=>'required'

        ]);
     
        $save=Subscription::updateOrcreate([
                
            'id'=>$r->id,
        ], [
            'name'=>$r->name,
         'plan_type'=>$r->plan_type, 
         'plan_cost_point'=>$r->point,
         'plan_cost_price'=>$r->price,
         'description'=>$r->description
    ]);
    if($save)
    return true;
    return false;
    }


}
