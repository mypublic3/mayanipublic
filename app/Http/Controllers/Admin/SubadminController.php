<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

use App\Models\User;
use App\Models\Country;
use App\Models\Artist;
use App\Models\AdminProfile;
use App\Helper\File;
use Illuminate\Support\Facades\Hash;
use DataTables;

use App\Notifications\SubadminNotification;
use Carbon\Carbon;
use Illuminate\Support\Str;
class SubadminController extends Controller
{
    use File;
    //
    public function profile(Request $r){
        $getprofile=User::where('id',$r->user()->id)->first();
        $country=Country::get();

        return view('admin.profile',compact('country'));   
     }
    public function subadmin(Request $r){
        if ($r->ajax()) {
            if($r->id != null){
                 return User::where('id',$r->id)->with('adminprofile')->with('country')->get()->first();
            }
       
            if($r->did!=null){
                $user = User::findOrFail($r->did);
              
                $user->delete();
                return true;
            }
         
            $data = User::where('role','subadmin')->with('adminprofile')->with('country')->get();

            return  Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('photo', function ($row) {

                   if($row->adminprofile){
                    $actionBtn = "<image width='80px' height='50px' class='img' onclick='openimage(\"" . asset('/storage'.$row->adminprofile['photo'])  . "\");'  src='" . asset('/storage'.$row->adminprofile['photo']) . "' />";
                   }
                   else{
                       $actionBtn="";
                   }
                    return $actionBtn;
                })
                ->addColumn('name',function($row){
                   if($row->adminprofile){
                    $name=$row->adminprofile['first_name'];
                   }
                   else{
                    $name="";
                   }

               


                        return $name;
               }) 
                ->addColumn('country',function($row){
                    return $row->country['nicename'];
               }) ->addColumn('mobile',function($row){
                $action=$row['mobile'];
                return $action;
           })
                ->addColumn('action',function($row){
                    $action = "
                    <button onclick='edit({$row['id']})' class=' btn btn-primary btn-sm'><i class='fa fa-edit'></i></button>
                    <button onclick='deletedata({$row['id']})' class=' btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return $action;
               })
                ->rawColumns(['photo','name','country','mobile','action'])
                ->make(true);
        }
        $country=Country::get();

        return view('admin.subadmin',compact('country'));
    }

    // save Genre 
    public function savesubadmin(Request $r){
        $this->validate($r, [
            'first_name'=>'required',
            'email'=>'required',
            'mobile'=>'required|numeric',
            'countrycode'=>'required'

        ]);
        $country = Country::where('phonecode', $r->countrycode)->first();

        $password=strtoupper(Str::random(8));

        $save=User::updateOrcreate([
                
            'id'=>$r->id,
        ], [
            'name'=>$r->name,
         'email'=>$r->email, 
         'mobile'=>$r->mobile,
         'password'=>Hash::make($password),
         'country_id'=>$country->id,
         'role'=>'subadmin',    
    ]);
    $subject='Mayani Music credentials';
    $msg = "Your password is " . $password;
    $link=url('/') . '/login';

     $getemail=User::where('email',$r->email)->first();
     $getemail->notify(new SubadminNotification(['subject' => $subject, 'msg' => $msg,'link'=>$link]));


     if($save) 
    $profile=AdminProfile::where('user_id',$r->user()->id)->first();

    if($r->has('photo')){  
        if($profile)

        if(is_file($profile->photo))
        unlink(public_path('storage'.$song->photo));        
        $filei = $r->file('photo');
        $pathi = 'artistImages';
        $urli = $this->file($filei,$pathi,300,400);
    }
    else{
        $urli=$profile->photo;
    }  
      $savesub=AdminProfile::updateOrcreate([
      'user_id'=>$save->id,
    ],[
        'first_name'=>$r->first_name,
        'last_name'=>$r->last_name,
        'email'=>$r->email,
        'country_code'=>$r->countrycode,
        'phone'=>$r->mobile,
        'address_line_one'=>$r->address_line_one,
        'address_line_two'=>$r->address_line_one,
        'country'=>$r->country,
        'state'=>$r->state,
        'city'=>$r->city,
        'zipcode'=>$r->zipcode,
        'photo'=>$urli,

    ]);

    $update=User::find($save->id);
    $update->role='subadmin';
    $update->save();
    if($save)
    return true;
    return false;
    }
}
