<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Country;
use App\Models\LoginMobile;

use Exception;
use Auth;
use Twilio\Rest\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\API\UserLoginRequest;
use App\Http\Requests\API\SavePrefrenceRequest;
use App\Http\Requests\API\MobileVerifyRequest;
use App\Http\Requests\API\CreateUserPassRequest;
use App\Http\Requests\API\CreateUserMobileRequest;
use App\Http\Requests\API\ResendOtpRequest;
use App\Http\Requests\API\ForgetPassRequest;
use App\Http\Requests\API\ForgetPassVerifyRequest;

use Illuminate\Support\Str;
use App\Models\UserPoint;







class AuthController extends Controller
{
    //
    public function countries(Request $r){
        $countries = Country::select("id", "name","iso","phonecode")->get();
        return $countries;
    }

 
    /**
     * userLogin
 
     * @bodyParam mobile integer required Enter Mobile number
     * @bodyParam password string required Enter password
     */

    public function userLogin(UserLoginRequest $r)
    {
       
        $valid = Auth::attempt(['mobile' => $r->mobile, 'password' => $r->password]);
        if ($valid) {
            $user = User::where('mobile', $r->mobile)->get()->first();
            if ($user->mobile_verified_at != null) {
                $token = $user->createToken('tokens')->plainTextToken;
                $user = $user->toArray();
                $getuserdata=User::with('profiledetail')->with(['pointdetail'=>function($query){
                    $query->select('id','user_id','points');
                }])->find($user['id']);
                return  ['success' => true, 'token' => $token, 'userdata' => $getuserdata];
            } else {
                return  ['success' => false, 'message' => 'Mobile Not Verified'];
            }
        }
        return  ['success' => false, 'message' => 'Invalid password'];
    }
     
    
    
    public function createuserMobile(CreateUserMobileRequest $r)
    {
      
        $userget = User::where('mobile', $r->mobile)->first();
        $country = Country::where('phonecode', $r->country)->first();
     
      
      

        
        if($userget){
           
            return ['success' => false, "message" => "User already registered"];
        
        }
        $getrefuser=User::where('ref_code',$r->ref_code)->first();
        if($getrefuser){
            $refuser=$getrefuser->id;
        }else{
            if($r->ref_code==null)
            return ['success' => false, "message" => "Referral Code Is Invalid"];
            $refuser=NULL;

        }

        $otp = rand(100000, 999999);
        // $otp=123456;
        $message='You otp for mobile verification is '.$otp.'';
        
        

        $user = LoginMobile::updateOrcreate([
            'mobile'=>$r->mobile,
        ],[
            'otp' => $otp,
            'country_id' => $country->id,
             'ref_userid'=>$refuser

        ]);

        $this->sendsms($r->mobile, $message);

     




        //send sms
        return ['success' => true,'otp'=>$otp, "message" => "Otp sent on  your mobile number successfully."];
    }

     
   
    /**
     * userVerify
     * @bodyParam mobile integer required Enter Mobile number
     * @bodyParam otp integer required Enter otp
     */

    public function mobileVerify(MobileVerifyRequest $r)
    {
       
        $user = LoginMobile::where('mobile', $r->mobile)->where('otp', $r->otp)->get()->first();
      
        if ($user) {
          
            $msg = "Mobile verified";
            $now= Carbon::now();
            $code=strtoupper(Str::random(8));

             User::updateOrcreate([
                  'mobile'=>$user->mobile,
             ],[
                'mobile_verified_at'=>$now,
                 'otp'=>$user->otp,
                 'country_id'=>$user->country_id,
                 'password'=>Hash::make(8765544335),
                 'ref_code'=>$code,
                 'ref_userid'=>$user->ref_userid,
             ]);
             $finduserpoint=UserPoint::where('user_id',$user->ref_userid)->first();

             if($finduserpoint){
                if($finduserpoint->total_ref_users==0){
                    $point=1;
                  }
                  elseif($finduserpoint->total_ref_users==1){
                    $point=2;

                }
                  elseif($finduserpoint->total_ref_users==4){
                      $point=5;
  
                  }
                  elseif($finduserpoint->total_ref_users==9){
                      $point=10;
                  }elseif($finduserpoint->total_ref_users==24){
                      $point=15;
                  }elseif($finduserpoint->total_ref_users==49){
                      $point=30;
                  }elseif($finduserpoint->total_ref_users==99){
                      $point=50;
                  }
                  elseif($finduserpoint->total_ref_users==199){
                      $point=100;
                  }
                  else{
                      $point=0;
                  }
                  $finduserpoint->points=$finduserpoint->points+$point;
                  $finduserpoint->referal_point=$finduserpoint->referal_point+$point;
                  $finduserpoint->total_ref_users=$finduserpoint->total_ref_users+1;


                  $finduserpoint->save();

               
             }
             else{
                 if($user->ref_userid!=null){
                 UserPoint::create(['user_id'=>$user->ref_userid,'points'=>1,'referal_point'=>1,'total_ref_users'=>1]);
                 }
                 

             }
           
            // unset($user['otp']);
            $user->ForceDelete();
            return  ['success' => true, 'message' => $msg];
        }
        return  ['success' => false, 'message' => 'Invalid otp'];
    }
    // forgot password api 
    public function forgetpass(ForgetPassRequest $r){
        $userget = User::where('mobile', $r->mobile)->first();
     
      
        $otp = rand(100000, 999999);
        // $otp=123456;
        $message='You otp for mobile verisfication is '.$otp.'';
         $this->sendsms($userget->mobile, $message);

            $userget->otp=$otp;
            $userget->save();
              
          
            return ['success' => true,'otp'=>$otp, "message" => "Otp sent on  your mobile number successfully."];
        
    }
    public function forgetpassverify(ForgetPassVerifyRequest $r){
    $user = User::where('mobile', $r->mobile)->where('otp', $r->otp)->get()->first();
    if($user)
    return ['success' => true, "message" => "Mobile Verified"]; 
    return ['success' => false,"message" => "Incorrect Otp"];


    }
    public function createuserPass(CreateUserPassRequest $r)
    {
      
        $user = User::where('mobile', $r->mobile)->get()->first();
        if ($user) {
            $user->password=Hash::make($r->password);
            $user->device_token=$r->device_token;
            $user->save();
            $token = $user->createToken('tokens')->plainTextToken;
            $user = $user->toArray();
            $getuserdata=User::with('profiledetail')->with(['pointdetail'=>function($query){
                $query->select('id','user_id','points');
            }])->find($user['id']);
            return  ['success' => true,'token'=>$token,'userdata'=>$getuserdata, 'message' => 'Account created'];


          
        }
        return  ['success' => false, 'message' => 'Invalid Mobile'];


    }

    public function resendotp(ResendOtpRequest $r){
       


                $otpp = rand(100000, 999999);
                // $otp=123456;

        $message='You otp for mobile verification is '.$otpp.'';
       $otp= $this->sendsms($r->mobile, $message);
       $user = User::where('mobile', $r->mobile)->first();
       $loginmobile = LoginMobile::where('mobile', $r->mobile)->first();

       if($user){
        $user->otp=$otpp;
        $user->save();
       }
       if($loginmobile){
        $loginmobile->otp=$otpp;
        $loginmobile->save();
       }

      

       return  ['success' => true, 'message' => 'Sent otp'];




    }
    public function updatetoken(Request $r){
        $this->validate($r, [
            'device_token' => 'required'


        ]);
        
        $user=User::findOrfail($r->user()->id);
        $user->device_token=$r->device_token;
        $user->save();
        return  ['success' => true,'message'=>'Device Token updated'];


        }

    // send sms 
    public function sendsms($mobile,$message)
    {
        $rn=''.$mobile.'';
        // $message="Testing message from abhishek";
       
        try {
  
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_TOKEN");
            $twilio_number = getenv("TWILIO_FROM");
            // $account_sid ="AC704abdf1c87aa3dc75bbd9c75c47cf8b";
            // $auth_token ="7b5fda4e2daa1e91e51e718b60e42e37";
            // $twilio_number ="+447830365424";
  
            $client = new Client($account_sid, $auth_token);

            $client->messages->create($rn, [
                'from' => $twilio_number, 
                'body' => $message]);
                $msg='OTP SMS Sent Successfully.';
  
            return $msg;
  
        } catch (Exception $e) {
            return $e->getMessage();
            // return dd("Error: ". $e->getMessage());
        }
    }



    

}
