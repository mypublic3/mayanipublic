<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Artist;
use App\Models\Genre;
use App\Models\Song;
use App\Models\Album;
use App\Models\AlbumSong;

use App\Models\Chart;
use App\Models\PlayList;
use App\Models\PlaylistSong;
use App\Models\UserPoint;
use App\Models\UserProfile;

use App\Models\SongPlay;

use RecursiveIteratorIterator;


use App\Models\UserPrefrence;
use App\Models\UserPlaylist;
use App\Models\CmsSetting;
use App\Models\Feedback;
use App\Models\AppNotification;
use App\Models\ShareQuote;
use App\Models\ArtistFollow;






use App\Http\Requests\API\SavePrefrenceRequest;
use App\Http\Requests\API\SongPlayRequest;
use App\Http\Requests\API\ArtistSongRequest;
use App\Http\Requests\API\PlaylistSongs;
use App\Http\Requests\API\AlbumSongs;
use App\Http\Requests\API\ChartSongRequest;
use App\Http\Requests\API\UserPlaylistRequest;
use App\Http\Requests\API\GenreSongRequest;
use App\Http\Requests\API\FeedbackRequest;
use App\Http\Requests\API\ArtistFollowRequest;

use DB;
use App\Http\Requests\API\DownloadRequest;
use App\Http\Requests\API\UpdateProfileRequest;
use App\Helper\File;
class HomeController extends Controller
{
    use File;

    //
    public function prefrences(Request $r){
        $artist=Artist::orderBy('created_at','desc')->get();
        $genre=Genre::orderBy('created_at','desc')->get();
        return ['success' => true,"artist" => $artist , "genre"=>$genre];

    }
    /**
 * Save user prefrences.
 *
 * @bodyParam   artist    array  required.
 * @bodyParam   genre    array  required  .
 *
 * @response {
 *  "access_token": "eyJ0eXA...",
 *  "token_type": "Bearer",
 * }
 */
        public function saveprefrence(SavePrefrenceRequest  $r){
            $user=$r->user()->id;
            $artist=json_encode($r->artist);

            $save=UserPrefrence::updateOrcreate([
                
                'user_id'=>$user,
            ], [
             'artist'=>json_encode($r->artist),
            'genre'=>json_encode($r->genre),
        ]);

        if($save)
        $updateuser=User::where('id',$user)->update(['is_prefrence'=>'1']);
        return ['success' => true,"message" => "Prefrences saved "];
        return ['success' => false,"message" => "Something went wrong ,Prefrences didn't saved "];


        }


        public function artistlist(Request $r){
            $artist = Artist::select('id','name','image')->get();
            return  ['success' => true, 'data' => $artist];

        }

        public function genrelist(Request $r){
            $genre = Genre::select('id','genre')->get();
            return  ['success' => true, 'data' => $genre];

        }
        public function songlist(Request $r){
            $songs = Song::select('id','name','image','track_file','artists','lyrics','stream_count')->with('isliked')->orderBy('created_at','desc')->get()->toArray();
            $res= array_map(function($each){
               
            if($each['isliked'] != null  ){
                $each['isliked'] =true;
           }
           else{
                $each['isliked'] =false;
           }
            return $each;


              
               
            },$songs);

            return  ['success' => true, 'songs' => $res];

        }
        public function albumlist(Request $r){
            $album= Album::select('id','name','image')->get();
            return  ['success' => true, 'data' => $album];

        }
        public function albumsongs(AlbumSongs $r){
            $albumdetail=Album::where('id',$r->album_id)->select('id','name','image','date')->get();
            $songs = AlbumSong::where('album_id',$r->album_id)->select('song_id')->with(['songdetail'=>function($query){
                $query->select('id','name','image','track_file','artists','lyrics','stream_count');
            }])->with('isliked')->get()->toArray();
            $res= array_map(function($each){
             
            if($each['isliked'] != []  ){
                $each['isliked'] =true;
           }
           else{
                $each['isliked'] =false;
           }
            return $each;


              
               
            },$songs);


            return  ['success' => true, 'album_detail' => $albumdetail, 'songs' => $res];

        }
        public function playlist(Request $r){
            $playlist= PlayList::select('id','name','genre','image')->get();
            return  ['success' => true, 'data' => $playlist];

        }
        public function playlistsongs(PlaylistSongs $r){
            $playlist=Playlist::where('id',$r->playlist_id)->select('id','name','image','genre')->get();
            $songs = PlaylistSong::where('playlist_id',$r->playlist_id)->select('song_id')->with(['songdetail'=>function($query){
                $query->select('id','name','image','track_file','artists','lyrics','stream_count');
            }])->with('isliked')->get()->toArray();
            $res= array_map(function($each){
               
            if($each['isliked'] != null  ){
                $each['isliked'] =true;
           }
           else{
                $each['isliked'] =false;
           }
            return $each;


              
               
            },$songs);

            return  ['success'=>true,'playlist_detail' => $playlist, 'songs' => $res];

        }

        public function chartlist(Request $r){
        //     $getchart=Chart::where('id',2)->pluck('songs');
        //     $getchart=Chart::where('id',2)->first('songs');


        //     $object = json_decode($getchart);
        //     $object = 2;
        //    return $json = json_encode($object);
        //        $getchart->songs=$json;
        //        $getchart->save();
            // return Chart::select('id','name','image','genre','no_of_songs','songs')->with('songdetail')->get();
            $charts= Chart::select('id','name','image','genre','no_of_songs')->get();

            return  ['success' => true, 'data' => $charts];

        }
        public function chartsongs(ChartSongRequest $r){
            $songs = Chart::where('id',$r->chart_id)->select('id','name','image','genre','songs')->with('songdetail')->with('isliked')->get()->toArray();
            $res= array_map(function($each){
          
            if($each['isliked'] != null  ){
                $each['isliked'] =true;
           }
           else{
                $each['isliked'] =false;
           }
            return $each;


              
               
            },$songs);

            return  ['success' => true, 'songs' => $res];


        }

        public function trendingsong(Request $r){
            $user=$r->user()->id;
            $songs =Song::select('id','name','image','track_file','artists','lyrics','stream_count')->with('isliked')->orderBy('stream_count','desc')->limit(10)->get()->toArray();
            $res= array_map(function($each){
                
            if($each['isliked'] != []  ){
                $each['isliked'] =true;
           }
           else{
                $each['isliked'] =false;
           }
            return $each;


              
               
            },$songs);


            return  ['success' => true, 'songs' => $res];
        }


     

        public function artistsongs(ArtistSongRequest $r){
            // ->whereJsonContains('players', '1')
            $gettartist=Artist::where('id',$r->artist_id)->select('id','name','image')->first();
            $getartist = Artist::where('id',$r->artist_id)->select('id','name','image')->withCount('followers')->with('isfollow')->get()->toArray();
            $artist= array_map(function($each){
               
                if($each['isfollow'] != []  ){
                    $each['isfollow'] ='Unfollow';
               }
               else{
                    $each['isfollow'] ='Follow';
               }
                return $each;
                },$getartist);
            $song= Song::whereJsonContains('artists_ids',$gettartist->id)->select('id','name','image','track_file','artists','lyrics','stream_count')->with('isliked')->get()->toArray();
            $res= array_map(function($each){
               
            if($each['isliked'] != []  ){
                $each['isliked'] =true;
           }
           else{
                $each['isliked'] =false;
           }
            return $each;


              
               
            },$song);


            $suggestedartist= Artist::select('id','name','image')->take(4)->get();
            return  ['success'=>true,'songs' => $res,'artist'=>$artist, 'suggested_artist' => $suggestedartist];

        }
        public function userplaylist(UserPlaylistRequest $r){
    
                $save=UserPlaylist::updateOrcreate([
                     'id'=>$r->id,
                ], [
                    'user_id'=>$r->user()->id,
                 'name'=>$r->name,
            ]);
            if($save)
            return ['success' => true,"message" => "Playlist Created "];
            return ['success' => false,"message" => "Something went wrong "];
    
    
            
    
        }

        public function getuserplaylist(Request $r){
            $playlist=UserPlaylist::where('user_id',$r->user()->id)->select('id','name')->get();
            return ['success' => true,"data" => $playlist];
            }

            public function getuserprefrence(Request $r){
                $prefrence=UserPrefrence::where('user_id',$r->user()->id)->select('id','user_id','artist','genre')->get();
                return ['success' => true,"data" => $prefrence];
            }

        // search api 
        public function search(Request $r){
            // ->whereJsonContains('players', '1')
            if($r->search==NULL)
            return ['success' => true, "songs" => [] ,"artist"=>[],"album"=>[],"playlist"=>[]];
            $song= Song::select('id','name','image','track_file','artists','lyrics','stream_count')->where('name','like',"%{$r->search}%")->get();
            $artist= Artist::select('id','name','image')->where('name','like',"%{$r->search}%")->get(); 
             $album= Album::select('id','name','image')->where('name','like',"%{$r->search}%")->get(); 
              $playlist= Playlist::select('id','name','image')->where('name','like',"%{$r->search}%")->get();

              return ['success' => true, "songs" => $song ,"artist"=>$artist,"album"=>$album,"playlist"=>$playlist];

        }

        public function recommendedsongs(Request $r){
            $prefrence=UserPrefrence::where('user_id',$r->user()->id)->get()->first();
            if(!$prefrence){
                return ['success' => false, "message" => "Prefrence didn't saved"];

            }
            $artist=json_decode($prefrence->artist);
            $genre=json_decode($prefrence->genre);
            $songs=Song::select('id','name','image','track_file','artists','lyrics','stream_count')->whereIn('genre',$genre)->with('isliked')->get()->toArray();
            $res= array_map(function($each){
            if($each['isliked'] != null  ){
                $each['isliked'] =true;
           }
           else{
                $each['isliked'] =false;
           }
            return $each;


              
               
            },$songs);
                        // $res=(object) $res;
                      

            return ['success' => true, "songs" => $res];

        }

        // genre songs api 
        public function genresongs(GenreSongRequest $r){
            $songs=Song::select('id','name','image','track_file','artists','lyrics','stream_count')->whereJsonContains('genre',$r->genre_id)->with('isliked')->get()->toArray();
            $res= array_map(function($each){
               
            if($each['isliked'] != null  ){
                $each['isliked'] =true;
           }
           else{
                $each['isliked'] =false;
           }
            return $each;


              
               
            },$songs);
                        // $res=(object) $res;
                      

            return ['success' => true, "songs" => $res];


        }

        public function songplay(SongplayRequest $r){
           
            $findsongplay=SongPlay::where('user_id',$r->user()->id)->where('song_id',$r->song_id)->first();
           if($findsongplay)
           {
               $playcount=$findsongplay->play_count + 1;
           } 
           else{
            $playcount=1;

           }
            $savesongplay=SongPlay::updateOrcreate([
                'user_id'=>$r->user()->id,
                'song_id'=>$r->song_id,      
                  ],[
                      'play_count'=>$playcount,
                  ]);
            $getuserspoint=UserPoint::where('user_id',$r->user()->id)->first();
            if($getuserspoint){
                if($getuserspoint->total_played_song==0){
                  $point=1;
                }
                elseif($getuserspoint->total_played_song==4){
                    $point=2;

                }
                elseif($getuserspoint->total_played_song==19){
                    $point=5;
                }elseif($getuserspoint->total_played_song==49){
                    $point=10;
                }elseif($getuserspoint->total_played_song==99){
                    $point=20;
                }elseif($getuserspoint->total_played_song==499){
                    $point=50;
                }
                elseif($getuserspoint->total_played_song==999){
                    $point=100;
                }elseif($getuserspoint->total_played_song==1999){
                    $point=200;
                }elseif($getuserspoint->total_played_song==4999){
                    $point=5000;
                }elseif($getuserspoint->total_played_song==9999){
                    $point=1000;
                }
                elseif($getuserspoint->total_played_song==49999){
                    $point=2000;
                }
                else{
                    $point=0;
                }
                UserPoint::updateOrcreate([
                    'user_id'=>$r->user()->id,
                    ],[
                        'play_point'=>$getuserspoint->play_point+$point,
                        'total_played_song'=>$getuserspoint->total_played_song+1,
                    ]);
            }         
            else{
                UserPoint::updateOrcreate([
                    'user_id'=>$r->user()->id,
                    ],[
                        'play_point'=>1,
                        'total_played_song'=>1,
                    ]);
            }

            $getartist=Song::where('id',$r->song_id)->get()->first();
            // $artist=$getartist->artists_ids;
            $artist=array_map('intval', json_decode($getartist->artists_ids));
            // foreach((array)$artist as $gt){
             $updateartiststream=Artist::whereIn('id',$artist)->update(['stream_count'=>DB::raw('stream_count+1')]);
             //update song stream count
             $getartist->stream_count=$getartist->stream_count+1;
             $getartist->save();
            // }
            return ['success' => true, "message" => "Song playes"];



        }

        public function downloadsong(DownloadRequest $r){
         $update=Song::where('id',$r->song_id)->update(['download_count'=>DB::raw('download_count+1')]);
         if($update) 
         return ['success' => true, "message" => "Song downloaded"];
         return ['success' => false, "message" => "Something went wrong!"];



        }

        public function aboutus(Request $r){
            $data=CmsSetting::where('user_id',1)->get()->first();
            if($data)
            return ['success' => true, "data" =>$data->aboutus];
         return ['success' => false, "message" => "Something went wrong!"];
        }
        public function termsconditions(Request $r){
            $data=CmsSetting::where('user_id',1)->get()->first();
            if($data)
            return ['success' => true, "data" =>$data->terms_conitions];
         return ['success' => false, "message" => "Something went wrong!"];
        }   
        public function privacypolicy(Request $r){
            $data=CmsSetting::where('user_id',1)->get()->first();
            if($data)
            return ['success' => true, "data" =>$data->privacy];
         return ['success' => false, "message" => "Something went wrong!"];
        }   public function legaldisclaimer(Request $r){
            $data=CmsSetting::where('user_id',1)->get()->first();
            if($data)
            return ['success' => true, "data" =>$data->legel_disclaimer];
         return ['success' => false, "message" => "Something went wrong!"];
        }

        public function userprofile(Request $r){
            $data=User::select('id','name','email','mobile')->where('id',$r->user()->id)->with(['profiledetail'=>function($query){
                $query->select('id','user_id','instagram','facebook','bio','gender','profile_image');
            }])->with(['pointdetail'=>function($query){
                $query->select('id','user_id','points');
            }])->get()->first();
            if($data)
            return ['success' => true, "data" =>$data];
            return ['success' => false, "message" => "Something went wrong!"];


        }
          
        public function updateprofileimg(Request $r){
            $this->validate($r, [
                'image' =>[Rule::requiredIf($r->id==null),'mimes:jpeg,png,jpg,gif,svg|max:2000'],    
            ]);
            $user=User::with('profiledetail')->find($r->user()->id);
            $userprofile=UserProfile::where('user_id',$r->user()->id)->first();
            if($r->has('image')){  
                if($userprofile){
                if(is_file($userprofile->profile_image)){
                unlink(public_path('storage'.$userprofile->profile_image)); 
                }   
                }    
                $filei   = $r->file('image');
                $pathi = 'userprofileImages';
                $urli = $this->file($filei,$pathi,300,400);
            }
            else{
                if($userprofile!=null){
                    if($userprofile->profile_image!=null){
                $urli=$userprofile->profile_image;
                    }
                }
                $urli=null;

            }
            $save=UserProfile::updateOrcreate([
                 'user_id'=>$r->user()->id,
            ],[
                'profile_image'=>$urli,
            ]);
            if($save)
            // {url('/')}
    return ['success' => true, "message" =>"Profile Updated!",'proofile_url'=>$urli];
    return ['success' => false, "message" => "Something went wrong!"];


        }
        public function updateprofile(UpdateProfileRequest $r){

            $user=User::with('profiledetail')->find($r->user()->id);
            $userprofile=UserProfile::where('user_id',$r->user()->id)->first();


         

            $save=User::updateOrcreate([
                
                'id'=>$r->user()->id,
            ], [
             'name'=>$r->name,
            'email'=>$r->email,
        ]);
        $savepro=UserProfile::updateOrcreate([
                
            'user_id'=>$r->user()->id,
        ], [
         'instagram'=>$r->instagram,
        'facebook'=>$r->facebook,
        'bio'=>$r->bio,  
        'gender'=>$r->gender,
    ]);
    $getuserdata=User::with('profiledetail')->with(['pointdetail'=>function($query){
        $query->select('id','user_id','points');
    }])->find($r->user()->id);
    if($save)
    return ['success' => true, "message" =>"Profile Updated!",'userdata'=>$getuserdata];
    return ['success' => false, "message" => "Something went wrong!"];


        }

        public function feedback(FeedbackRequest $r){
            $save=Feedback::create([
               'user_id'=>$r->user()->id,
            'topic'=>$r->topic,
            'feedback'=>$r->feedback
       ]);
       if($save)
       
    return ['success' => true, "message" =>"Feedback Submitted!"];
    return ['success' => false, "message" => "Something went wrong!"];

        }
        public function notifications(Request $r){
            $data=AppNotification::get();
            return ['success' => true, "data" =>$data];



        }
        public function sharecontent(Request $r){
            $data=ShareQuote::select('content')->first();
            return ['success' => true, "content" =>$data];



        }
        public function followartist(ArtistFollowRequest $r){
            $getiffollows=ArtistFollow::where('user_id',$r->user()->id)->where('artist_id',$r->artist_id)->first();
            if($getiffollows){
                $getiffollows->delete();
                return ['success' => true, "message" =>"Artist Unfollowed"];

            }

            $save=ArtistFollow::create([
               'user_id'=>$r->user()->id,
            'artist_id'=>$r->artist_id
       ]);
       if($save)
       
    return ['success' => true, "message" =>"Artist Followed"];
    return ['success' => false, "message" => "Something went wrong!"];

        }
        
        
      

        public function logout(Request $r){
            $r->user()->currentAccessToken()->delete();
            return ['success' => true, "message" => "Logged Out Successfully,"];

        }
}
