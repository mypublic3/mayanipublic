<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Country;
use App\Models\Artist;
use App\Models\Album;
use App\Models\Playlist;
use App\Models\Chart;
use App\Models\LikePlaylist;



use Exception;
use Auth;
use Twilio\Rest\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Models\LikeSong;
use App\Models\Song;
use App\Models\UserPlaylist;
use App\Models\UserPlaylistSong;
use App\Http\Requests\API\LikeSongRequest;
use App\Http\Requests\API\UserPlaylistSongRequest;
use App\Http\Requests\API\RemoveUserpRequest;
use App\Http\Requests\API\RemoveUserSongRequest;
use App\Http\Requests\API\CheckLikeSongRequest;
use App\Http\Requests\API\CheckPlaylistLikeRequest;
use App\Http\Requests\API\LikePlaylistRequest;









class UserPlaylistController extends Controller
{
    //

    public function userlikedsong(Request $r){
         $songs=LikeSong::where('user_id',$r->user()->id)->with(['songdetail'=>function($query){
            $query->select('id','name','image','track_file','artists','lyrics','stream_count');
        }])->with('isliked')->get()->toArray();
        $res= array_map(function($each){
           
        if($each['isliked'] == []){
            $each['isliked'] = true;
       }
       else{
            $each['isliked'] = true;
       }
        return $each;
    
    
          
           
        },$songs);

        return  ['success'=>true,'data' => $songs];

    }

    // api for user to like a song 
    
    public function likesong(LikeSongRequest $r){
        $ifget=LikeSong::where('user_id',$r->user()->id)->where('song_id',$r->song_id)->first();
        if($ifget){
        $ifget->delete();
        return ['success' => true,"message" => "Song Unliked  "];
        }
    
        $save=LikeSong::updateOrcreate([
             'user_id'=>$r->user()->id,
              'song_id'=>$r->song_id
        ]);
    if($save)
    return ['success' => true,"message" => "Song Liked  "];
    return ['success' => false,"message" => "Something went wrong "];


    

}
//    add song to playlist 
public function addplaylistsong(UserPlaylistSongRequest $r){
    
    $save=UserPlaylistSong::updateOrcreate([
          'song_id'=>$r->song_id,
          'playlist_id'=>$r->playlist_id
    ]);
    $getlist=UserPlaylist::find($r->playlist_id);

if($save)
$getlist->songs=$getlist->songs+1;
$getlist->save();

return ['success' => true,"message" => "Song Added To Playlist  "];
return ['success' => false,"message" => "Something went wrong "];

}

// get user's playlists songs 


public function userplaylistsong(Request $r){
    $user=$r->user()->id;
    $getplaylist=UserPlaylist::where('id',$r->playlist_id)->get()->first();
    $songs=UserPlaylistSong::select('id','playlist_id','song_id')->where('playlist_id',$r->playlist_id)->with(['songdetail'=>function($query){
        $query->select('id','name','image','track_file','artists','lyrics','stream_count');
    }])->with('isliked')->get()->toArray();
    $res= array_map(function($each){
       
    if($each['isliked'] != []  ){
        $each['isliked'] =true;
   }
   else{
        $each['isliked'] =false;
   }
    return $each;


      
       
    },$songs);

    if($songs)
    return  ['success'=>true,'playlist_detail'=>$getplaylist,'songs' => $res];
    return  ['success'=>true,'playlist_detail'=>$getplaylist,'songs' => $res];


}
        public function removeuserplaylist(RemoveUserpRequest $r){
            $data=UserPlaylist::find($r->playlist_id);
            $data->delete();
            
    return  ['success'=>true,'message' => 'Playlist Deleted'];
    return  ['success'=>false,'message' => 'Playlist Not Found'];


        }
        public function removeuserplaylistsong(RemoveUserSongRequest $r){
            $data=UserPlaylistSong::where('playlist_id',$r->playlist_id)->where('song_id',$r->song_id)->delete();
            if($data)            
    return  ['success'=>true,'message' => 'Song Deleted'];
    return  ['success'=>false,'message' => 'Playlist Song Not Found'];


        }

        public function checklikesong(CheckLikeSongRequest $r){
            $checklike=LikeSong::where('user_id',$r->user()->id)->where('song_id',$r->song_id)->first();
            if($checklike)
    return  ['success'=>true];
    return  ['success'=>false];

        }

        // api for like a playlist or album

        public function likeplaylists(LikePlaylistRequest $r)
        {
            // $check1=Artist::find($r->id);
            // $check2=Album::find($r->id);
            // $check3=Playlist::find($r->id);
            // $check4=Chart::find($r->id);
            // if(!$check1 || !$check2 || !$check3 || !$check4 ){
            //     return  ['success' => false, 'message' => 'Given Id is invalid'];
            // }

            //  true is for like and false is for not liked 
            $save=LikePlaylist::updateOrcreate([
                'user_id'=>$r->user()->id,
                'common_id'=>$r->id,
                'type'=>$r->type

            ],[
                'status'=>$r->status,

          ]);
          if($save)
          if($r->status==1){
            return  ['success' => true, 'message' => ''.$r->type.' liked'];

          }
          if($r->status==0){
            return  ['success' => true, 'message' => ''.$r->type.' Unliked'];

          }
          return  ['success' => false, 'message' => 'Something went wrong'];



        }

        public function getlikeplaylists(Request $r){
            $data=LikePlaylist::where('status','1')->select('id','user_id','common_id','type')->where('user_id',$r->user()->id)->get();
            return  ['success' => true, 'list' => $data];


        }

        // api for check users liked playlist album chart artist 
        public function checklikeplaylist(CheckPlaylistLikeRequest $r){
            $data=LikePlaylist::where('status','1')->where('user_id',$r->user()->id)->where('common_id',$r->id)->where('type',$r->type)->first();
            if($data)
            return  ['success' => true];
            return  ['success' => false];



        }

        // api for delte function 
        public function deleteuser(Request $r){
          $user=User::find($r->user()->id);
          $user->forceDelete();
          return  ['success' => true];
          return  ['success' => false];
        }

    }
